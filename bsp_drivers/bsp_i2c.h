#ifndef __BSP_I2C_H
#define __BSP_I2C_H
#include "stm32f4xx_hal.h"

typedef struct {
	uint8_t dev_addr;//设备地址，包含读写
	uint16_t r_w_addr;//写入的地址
	uint8_t *buf;//读写数据内容
	uint16_t len;
}I2C_MSG; 
extern void at24cxx_read(I2C_MSG *msg);
extern void at24cxx_write(I2C_MSG *msg);
extern int i2c_init(void);
extern int IIC_writebyte(uint8_t dev_addr,uint16_t write_addr,uint8_t DataToWrite);
extern uint8_t IIC_readbyte(uint8_t dev_addr,uint16_t read_addr);
#endif




