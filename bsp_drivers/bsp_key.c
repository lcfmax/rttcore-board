#include "bsp_key.h"
#include "config.h"
#include "bsp_led.h"
#define device  struct rt_device
#define KEY0_VALUE PIN_IN(E,4)
#define WE_UP_VALUE PIN_IN(A,0)
static rt_thread_t key = RT_NULL;

int key_init()
{
	 __HAL_RCC_GPIOA_CLK_ENABLE();
	 __HAL_RCC_GPIOE_CLK_ENABLE();
	 GPIO_InitTypeDef GPIO_InitStruct = {0};
	 GPIO_InitStruct.Mode=GPIO_MODE_IT_RISING;
	 GPIO_InitStruct.Pin=GPIO_PIN_4;
	 GPIO_InitStruct.Pull=GPIO_PULLDOWN;
	 GPIO_InitStruct.Speed=GPIO_SPEED_FREQ_HIGH;
	 HAL_GPIO_Init(GPIOE,&GPIO_InitStruct);
	 GPIO_InitStruct.Pin=GPIO_PIN_0;
	 HAL_GPIO_Init(GPIOA,&GPIO_InitStruct);
	 HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
	 HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	 HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
	 HAL_NVIC_EnableIRQ(EXTI4_IRQn);
	 return true;
}

rt_err_t key_close(device *dev)
{
	__HAL_RCC_GPIOA_CLK_DISABLE();
	__HAL_RCC_GPIOE_CLK_DISABLE();
	HAL_GPIO_DeInit(GPIOA,GPIO_PIN_0);
	HAL_GPIO_DeInit(GPIOE,GPIO_PIN_14);
	rt_thread_delete(key);
	return RT_EOK;
}



