#include "config.h"

uint8_t TP_Init(void)
{
	GPIO_InitTypeDef GPIO_Initure;

	__HAL_RCC_GPIOB_CLK_ENABLE();			//开启GPIOB时钟
	__HAL_RCC_GPIOC_CLK_ENABLE();			//开启GPIOC时钟
	__HAL_RCC_GPIOF_CLK_ENABLE();			//开启GPIOF时钟
	
	//GPIOB1,2初始化设置
	GPIO_Initure.Pin=GPIO_PIN_1|GPIO_PIN_2;	//PB1/PB2 设置为上拉输入
	GPIO_Initure.Mode=GPIO_MODE_INPUT;  	//输入模式
	GPIO_Initure.Pull=GPIO_PULLUP;          //上拉
	GPIO_Initure.Speed=GPIO_SPEED_HIGH;     //高速
	HAL_GPIO_Init(GPIOB,&GPIO_Initure);     //初始化
	
	//PB0
	GPIO_Initure.Pin=GPIO_PIN_0; 			//PB0设置为推挽输出
	GPIO_Initure.Mode=GPIO_MODE_OUTPUT_PP;  //推挽输出
	HAL_GPIO_Init(GPIOB,&GPIO_Initure);     //初始化
	
	//PC13
	GPIO_Initure.Pin=GPIO_PIN_13;          	//PC13设置为推挽输出
	HAL_GPIO_Init(GPIOC,&GPIO_Initure);     //初始化
	
	//PF11
	GPIO_Initure.Pin=GPIO_PIN_11;          	//PF11设置推挽输出
	HAL_GPIO_Init(GPIOF,&GPIO_Initure);     //初始化			

	TP_Read_XY(&tp_dev.x[0],&tp_dev.y[0]);//第一次读取初始化	 
	AT24CXX_Init();		//初始化24CXX
	if(TP_Get_Adjdata())return 0;//已经校准
	else			   //未校准?
	{ 										    
		LCD_Clear(WHITE);//清屏
		TP_Adjust();  	//屏幕校准 
		TP_Save_Adjdata();	 
	}			
	TP_Get_Adjdata();	
	}
	return 1; 									 
}