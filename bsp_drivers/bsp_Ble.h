#ifndef __BSP_BLE_H
#define __BSP_BLE_H
#include "stm32f4xx_hal.h"
#include "config.h"

#define STA 	PIN_IN(A,1)
#define WKUP 	PIN_OUT(A,7)

typedef struct {
    char 		*cmd;
    char 		*true_ack;
    char 		*err_ack;
    uint32_t 	timeout;//超过设定时间收不到数据则直接结束
    uint8_t		retry_times;//收到错误字符，重试次数
} BLE_TABLE;

typedef struct {
	uint8_t     ready_read;//可以读取数据
   struct       rt_ringbuffer *rx_ringbuffer;
	uint16_t 	recv_len;		//接收的数据长度
} BLE_RECV_DATA;//接收的数据


#define BLE_RINGBUFFER_MAX 200
typedef struct {
    struct 	rt_device 	dev;								//BLE设备
    rt_device_t 		uart_dev;							//BLE所用串口设备
	
    rt_thread_t 		Ble_tx_tid;							//BLE发送线程
    rt_thread_t 		Ble_rx_tid;							//BLE接收线程
    rt_sem_t    		tx_sem;								//BLE发送数据信号量
	rt_sem_t			read_sem;							//BLE读取数据信号量
    rt_timer_t  		Ble_timeOut_timer;					//BLE初始化过程无数据接收超时定时器
    uint8_t 			timeout_flag;						//BLE超时标志
    uint8_t     		AT_index;							//BLE AT命令状态表索引
    uint8_t 			*read_buf;							//BLE接收数据指针
    uint8_t				step;								//BLE初始化步骤 0-发送命令 1-等待回应 2-初始化完成，接收数据 3-初始化失败
	uint8_t				*tx_data_point;						//BLE发送数据指针
	uint8_t				tx_data_len;						//BLE发送的数据长度
	uint8_t 			tx_finish_flag;						//BLE发送数据成功标志
    BLE_RECV_DATA       Ble_recv_data[BLE_RINGBUFFER_MAX];   
	BLE_TABLE 		*Ble_table;						//BLE初始化状态表
} DEV_BLE;

#define GET_BLE_RX_LEN		0
#define GET_BLE_TX_STATE	1
#define REGISTER_BLE_FUN	2
#define QUERY_CONNECT_IP_PORT	3
#endif




