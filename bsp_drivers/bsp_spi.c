#include "bsp_spi.h"
#include "config.h"

SPI_HandleTypeDef hspi1;
#define   SPI_CS_ENABLE		PIN_OUT(B,14)=0
#define   SPI_CS_DISABLE	PIN_OUT(B,14)=1
#define   SPI_CAPCITY		128/8*1024*1024
int MX_SPI1_Init(void)
{
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */
  __HAL_SPI_ENABLE(&hspi1);
  SPI1_RW_Byte(0xff);  
  return true;
}

void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(hspi->Instance==SPI1)
  {
    __HAL_RCC_SPI1_CLK_ENABLE();

    __HAL_RCC_GPIOB_CLK_ENABLE();
    /**SPI1 GPIO Configuration
    PB3     ------> SPI1_SCK
    PB4     ------> SPI1_MISO
    PB5     ------> SPI1_MOSI
    */
    GPIO_InitStruct.Pin = GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /* SPI1 interrupt Init */
//    HAL_NVIC_SetPriority(SPI1_IRQn, 0, 0);
//    HAL_NVIC_EnableIRQ(SPI1_IRQn);
  }

}

/**
* @brief SPI MSP De-Initialization
* This function freeze the hardware resources used in this example
* @param hspi: SPI handle pointer
* @retval None
*/
void HAL_SPI_MspDeInit(SPI_HandleTypeDef* hspi)
{
  if(hspi->Instance==SPI1)
  {
    __HAL_RCC_SPI1_CLK_DISABLE();
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_3|GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_14);
    HAL_NVIC_DisableIRQ(SPI1_IRQn);
  }
}
//PLCK2/per  PLCK���84MHZ
void Set_SPI1_Speed(uint8_t speed )
{
	assert_param(IS_SPI_BAUDRATE_PRESCALER(SPI_BaudRatePrescaler));
	__HAL_SPI_DISABLE(&hspi1);
	hspi1.Instance->CR1&=0xFFC7;
	hspi1.Instance->CR1|=speed;
	__HAL_SPI_ENABLE(&hspi1); //ʹ�� SPI
}
uint8_t SPI1_RW_Byte(uint8_t tx_byte)
{
	uint8_t rx_byte=0;
	HAL_SPI_TransmitReceive(&hspi1,&tx_byte,&rx_byte,1,1000);
	return rx_byte;
}

void SPI1_IRQHandler(void)
{
  rt_interrupt_enter();
  HAL_SPI_IRQHandler(&hspi1);
  rt_interrupt_leave();
}









