#include "bsp_spiflash.h"
#include "bsp_spi.h"
#define device  struct rt_device
device spi_flash;
extern SPI_HandleTypeDef hspi1;
/*
W25Q128 128Mbit即16M字节容量，分为256个块，每个块64K字节
每个块分为16个扇区，每个扇区4K字节，16个页，擦除最小单位4K
地址范围0-0xFFFFFF
*/
#define   SPI_CS_ENABLE		PIN_OUT(B,14)=0
#define   SPI_CS_DISABLE	PIN_OUT(B,14)=1
uint8_t spi_init_flag=0;
rt_err_t spi_flash_init(device *dev)
{
	if(spi_init_flag==1) return true;
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/*Configure GPIO pin : PB14 */
	GPIO_InitStruct.Pin = GPIO_PIN_14;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	SPI_CS_DISABLE;
	MX_SPI1_Init();
	spi_init_flag=1;
	return RT_EOK;
}

void spi_write_enable()
{
	SPI_CS_ENABLE;
	SPI1_RW_Byte(WRITE_ENABLE);
	SPI_CS_DISABLE;
}

void spi_write_disable()
{
	SPI_CS_ENABLE;
	SPI1_RW_Byte(WRITE_DISABLE);
	SPI_CS_DISABLE;
}
//flashid 1752H
uint16_t spi_read_id()
{
	uint16_t id=0;
	SPI_CS_ENABLE;
	SPI1_RW_Byte(READ_FLASH_ID);
	SPI1_RW_Byte(0);
	SPI1_RW_Byte(0);
	SPI1_RW_Byte(0);
	id=(SPI1_RW_Byte(0xff)<<8)|SPI1_RW_Byte(0xff);
	SPI_CS_DISABLE;
	return id;
}
/*
S7 S6 S5 S4 S3 S2 S1 S0
SRP0 SEC TB BP2 BP1 BP0 WEL BUSY
S15 S14 S13 S12 S11 S10 S9 S8
SUS CMP LB3 LB2 LB1 (R) QE SRP1
*/
uint8_t spi_read_status(uint8_t index)
{
	uint16_t status=0;
	SPI_CS_ENABLE;
	if(index==1)
	{
		SPI1_RW_Byte(READ_STATUS_REG1);
		status=SPI1_RW_Byte(0xff);
		SPI_CS_DISABLE;
		return status;
	}	
	else if(index==2)
	{
		SPI1_RW_Byte(READ_STATUS_REG2);
		status=SPI1_RW_Byte(0xff);
		SPI_CS_DISABLE;
		return status;		
	}
	return 0xff;		
}
void spi_write_status(uint8_t index,uint8_t sr)
{
	SPI_CS_ENABLE;
	spi_write_enable();
	if(index==1)
	{
		SPI1_RW_Byte(WRITE_STATUS_REG);
		SPI1_RW_Byte(sr);
		SPI_CS_DISABLE;
		return;
	}	
	else if(index==2)
	{
		SPI1_RW_Byte(WRITE_STATUS_REG);
		SPI1_RW_Byte(sr);
		SPI_CS_DISABLE;
		return;		
	}		
}
uint8_t Get_SPI_BUSY()
{
	return (spi_read_status(1)&0x01);
}
void wait_busy()
{
	while(Get_SPI_BUSY()==1);
}
void SPI_sector_erased(uint32_t sector_addr)
{
	sector_addr*=4096;
    spi_write_enable();                  //SET WEL 	 先写使能
	wait_busy();   
  	SPI_CS_ENABLE;                            //使能器件   ，再片选
    SPI1_RW_Byte(SECTOR_ERASE_4K);   //发送扇区擦除指令 
    SPI1_RW_Byte((uint8_t)((sector_addr)>>16));  //发送24bit地址    
    SPI1_RW_Byte((uint8_t)((sector_addr)>>8));   
    SPI1_RW_Byte((uint8_t)sector_addr);  
	SPI_CS_DISABLE;                         //取消片选     	      
	wait_busy();  				    //等待擦除完成
}
void SPI_chip_erased()
{
	wait_busy();
	SPI_CS_ENABLE;
	spi_write_enable();
	SPI1_RW_Byte(CHIP_ERASE);
	SPI_CS_DISABLE;
	wait_busy();
}
void write_page_program(int addr,uint8_t *buf,int len)
{
	int i=0;
	if(len>256) return;
	spi_write_enable();
	SPI_CS_ENABLE;
	
	SPI1_RW_Byte(PAGE_POGRAM);
	SPI1_RW_Byte((addr>>16)&0xff);
	SPI1_RW_Byte((addr>>8)&0xff);
	SPI1_RW_Byte(addr&0xff);
	for(i=0;i<len;i++){
		SPI1_RW_Byte(buf[i]);
	}
	SPI_CS_DISABLE;
	wait_busy();
}
void SPI_Write_NoCheck(int addr,uint8_t *buf,int len)
{
	uint16_t page_remain=0;//计算页剩余字节
	page_remain=256-addr%256;
	if(len<=page_remain)	page_remain=len;
	while(1)
	{
		write_page_program(addr,buf,page_remain);
		if(page_remain==len) break;
		else
		{
			addr+=page_remain;
			buf+=page_remain;
			len-=page_remain;
			if(len>256) page_remain=256;
			else page_remain=len;
		}
	}
}
void Power_down()
{
	SPI_CS_ENABLE;
	SPI1_RW_Byte(POWERDOWN);
	SPI_CS_DISABLE;
	delay_us(3);
}
void wakeup()
{
	SPI_CS_ENABLE;
	SPI1_RW_Byte(REALEASEPOWERDOWN);
	SPI_CS_DISABLE;
	delay_us(3);
}
rt_size_t SPI1_Read(device *dev,rt_off_t addr,void *buf,rt_size_t len)
{
	if((addr&0x00ffffff)+len>=SPI_CAPCITY) return false;
	uint8_t *point=(uint8_t*)buf;
	int i=0;
	SPI_CS_ENABLE;
	SPI1_RW_Byte(READ_DATA);
	SPI1_RW_Byte((uint8_t)(addr>>16));
	SPI1_RW_Byte((uint8_t)(addr>>8));
	SPI1_RW_Byte((uint8_t)(addr));
	for(i=0;i<len;i++)
	{
		*point=SPI1_RW_Byte(0xff);
		point++;
	}
		
	SPI_CS_DISABLE;
	
	return true;
}
uint8_t W25QXX_BUFFER[4096]={0};
rt_size_t SPI1_Write(device *dev,rt_off_t addr,const void *buf,rt_size_t len)
{
	if((addr&0x00ffffff)+len>=SPI_CAPCITY) return false;
	uint8_t *point=(uint8_t*)buf;
	uint32_t secpos;
	uint16_t secoff;
	uint16_t secremain;	   
 	uint16_t i;    
	uint8_t * W25QXX_BUF;	  
   	W25QXX_BUF=W25QXX_BUFFER;	     
 	secpos=addr/4096;//扇区地址  
	secoff=addr%4096;//在扇区内的偏移
	secremain=4096-secoff;//扇区剩余空间大小   
 	if(len<=secremain)secremain=len;//不大于4096个字节
	while(1) 
	{	
		SPI1_Read(dev,secpos*4096,W25QXX_BUF,4096);//读出整个扇区的内容
		for(i=0;i<secremain;i++)//校验数据
		{
			if(W25QXX_BUF[secoff+i]!=0XFF)break;//需要擦除  	  
		}
		if(i<secremain)//需要擦除
		{
			SPI_sector_erased(secpos);//擦除这个扇区
			for(i=0;i<secremain;i++)	   //复制
			{
				W25QXX_BUF[i+secoff]=point[i];	  
			}
			SPI_Write_NoCheck(secpos*4096,W25QXX_BUF,4096);//写入整个扇区  

		}else SPI_Write_NoCheck(addr,point,secremain);//写已经擦除了的,直接写入扇区剩余区间. 				   
		if(len==secremain)break;//写入结束了
		else//写入未结束
		{
			secpos++;//扇区地址增1
			secoff=0;//偏移位置为0 	 

		   	point+=secremain;  //指针偏移
			addr+=secremain;//写地址偏移	   
		   	len-=secremain;				//字节数递减
			if(len>4096)secremain=4096;	//下一个扇区还是写不完
			else secremain=len;			//下一个扇区可以写完了
		}	 
	};
	
	return true;
}
uint8_t SPI_ISReady()
{
	while(spi_read_id()!=0x1752);
	return true;
}
rt_err_t SPI1_Contorl(device *dev,int cmd,void *args)
{
	
	switch(cmd)
	{
		case  W25Q_READ_FLASH_ID:
			*(uint16_t*)args=spi_read_id();
			break;
		case  W25Q_WRITE_ENABLE :
			spi_write_enable();
			break;
		case  W25Q_WRITE_DISABLE:
			spi_write_disable();
			break;
		case  W25Q_READ_STATUS_REG1:
			*(uint8_t*)args=spi_read_status(1);
			break;
		case  W25Q_READ_STATUS_REG2:
			*(uint8_t*)args=spi_read_status(2);
			break;
		case  W25Q_WRITE_STATUS_REG1:
			spi_write_status(1,1);
			break;
		case  W25Q_WRITE_STATUS_REG2:
			spi_write_status(1,2);
			break;
		case  W25Q_CHIP_ERASE:
			SPI_chip_erased();
			break;
		case  W25Q_READ_DATA:
			break;
		case   W25Q_POWERDOWN:
			Power_down();
			break;
		case   W25Q_REALEASEPOWERDOWN:
			wakeup(); 
		case   W25Q_READY:
			SPI_ISReady(); 
			break;
	}
	
	return RT_EOK;
}
int spi_flash_register_dev()
{
	spi_flash.read=SPI1_Read;
	spi_flash.write=SPI1_Write;
	spi_flash.control=SPI1_Contorl;
	spi_flash.init=spi_flash_init;
	return rt_device_register(&spi_flash,"spiflash",RT_DEVICE_FLAG_RDWR); 
}

DEVICE_INIT(spi_flash_register_dev);


















