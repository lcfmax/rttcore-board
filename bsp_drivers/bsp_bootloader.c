#include "bsp_bootloader.h"
#include "config.h"
#include "bsp_wifi.h"

#define BOOT_INDEX	0
#define PARAM_INDEX	1
#define APP_INDEX	2
#define DOWN_INDEX	3
typedef void (*jump_app)(void);
void YModem_Wifi_Handle(char *InBuf,int len);
jump_app iap_fun;
uint32_t APP_ADDR=0;
struct PART_TABLE part_table[]={
		{"bootloader",0x08000000,(16*4+64+128)*1024},//bootloader
		{"param",0x08040000,128*1024},//参数
		{"APP",0x08060000,2*128*1024},//APP
		{"download",0x080A0000,2*128*1024},//download
};
void bootloader_init(void)
{
	APP_ADDR=part_table[APP_INDEX].start_addr;
//	register_data_handle_fun((uint32_t)YModem_Wifi_Handle);
}
void clear_boot_status()
{
	APP_ADDR=part_table[APP_INDEX].start_addr;
}
int copy_download_to_app(int len)
{
	uint8_t buf[32]={0};
	int i=0;
	uint32_t app_addr=part_table[APP_INDEX].start_addr;
	
	uint32_t down_addr=part_table[DOWN_INDEX].start_addr;
	if(len>part_table[APP_INDEX].size)
		return RT_ERROR;
	if(stm32_flash_erase(app_addr,part_table[APP_INDEX].size)!=RT_EOK)
		return RT_ERROR;
	if((len%32)!=0)
		len+=len%32;
	for(i=0;i<len;i+=32)
	{
		if(stm32_flash_read(down_addr,buf,32)!=RT_EOK)
			return RT_ERROR;
		if(stm32_flash_write(app_addr,buf,32)!=RT_EOK)
			return RT_ERROR;
		memset(buf,0,32);
		down_addr+=32;
		app_addr+=32;
	}
	return RT_EOK;
}

int write_data_to_App(uint8_t *buf,int len)
{
	if(APP_ADDR>=part_table[DOWN_INDEX].start_addr)
		return RT_ERROR;
	if(stm32_flash_write(APP_ADDR,buf,len)!=RT_EOK)
			return RT_ERROR;
	APP_ADDR+=len;
	return true;
}


void jump_to_app()
{
	uint32_t addr=part_table[APP_INDEX].start_addr;
	if(((*(__IO uint32_t*)addr)&0x2FFE0000)==0x20000000)	//检查栈顶地址是否合法.
	{ 
		iap_fun=(jump_app)*(__IO uint32_t*)(addr+4);		//用户代码区第二个字为程序开始地址(复位地址)		
		MSR_MSP(*(__IO uint32_t*)addr);					//初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
		iap_fun();									//跳转到APP.
	}
}	


void YModem_Wifi_Handle(char *InBuf,int len)
{
	
}













