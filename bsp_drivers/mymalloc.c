#include "mymalloc.h"
#include "bsp_sram.h"

	
static struct rt_memheap _heap_Sram;

int mem_init()
{
	#if defined(RT_USING_MEMHEAP)

	rt_memheap_init(&_heap_Sram,"sram",
                    (void*)Bank1_SRAM3_ADDR,
                    (rt_uint32_t)Bank1_SRAM3_SIZE);
	#endif
	return 1;
}

rt_err_t mem_detach()
{
	return rt_memheap_detach(&_heap_Sram);
}
void *my_alloc(int size)
{
	void *alloc_addr=0;
    alloc_addr=rt_memheap_alloc(&_heap_Sram,size);
	rt_memset(alloc_addr,0,size);
	return alloc_addr;
}
void *my_realloc(void *ptr,int size)
{
	return rt_memheap_realloc(&_heap_Sram,ptr,size);
}
void my_free(void *ptr)
{
	rt_memheap_free(ptr);
}
void my_memset(void *s, int c, int count)
{
	rt_memset(s,c,count);
}











