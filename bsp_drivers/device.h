#ifndef _DEVICE_H
#define _DEVICE_H
#include "stdint.h"
#include "string.h"
#include "rtthread.h"

#define NAME_MAXLEN 30

void auto_init(void);

/*
__attribute__((used,__section__(".fn_cmd."level)))中used的意思。
unused：表示该函数或变量可能不使用，这个属性可以避免编译器产生警告信息。
used： 向编译器说明这段代码有用，即使在没有用到的情况下编译器也不会警告。
*/
typedef int (*device_auto_init)(void);
#define AUTO_SECTION(level)  	__attribute__((used,__section__(".fn_cmd."level)))
#define DEVICE_START_EXPORT(func) 	const 	device_auto_init 	cmd_fn_##func AUTO_SECTION("0.end")=func
#define DEVICE_END_EXPORT(func)  	const	device_auto_init	cmd_fn_##func AUTO_SECTION("1.end")=func
#define APP_END_EXPORT(func)  		const	device_auto_init	cmd_fn_##func AUTO_SECTION("2.end")=func
	
#define DEVICE_INIT(func)  			const 	device_auto_init 	cmd_fn_##func AUTO_SECTION("1")=func
#define APP_INIT(func)  			const 	device_auto_init 	cmd_fn_##func AUTO_SECTION("2")=func	

#ifdef MY_DEVICE
typedef struct my_device device;
__packed struct list_node{
	struct list_node *next;
	struct list_node *prev;
};

typedef struct list_node list;

__packed struct object{
    char *name;
	uint16_t Dev_Num;
	list device_list;
};

__packed struct my_device{
	struct object parent;
	int (*init)(device *dev);
	int (*open)(device *dev);
	int (*close)(device *dev);
	int (*contorl)(device *dev,int cmd,void *args);
	int (*read)(device *dev,int offest,void *buf,int len);
	int (*write)(device *dev,int offest,const void *buf,int len);
	
	void *args;
	rt_sem_t lock_sem;
	uint8_t init_flag;
};
typedef struct object *obj;
inline void list_init(list *l)
{
	l->next=l->prev=l;
}

inline void list_insert_after(list *l,list *n)
{
	l->next->prev=n;
	n->next=l->next;
	
	l->next=n;
	n->prev=l;
}
inline void list_insert_before(list *l,list *n)
{
	l->prev->next=n;
	n->prev= l->prev;
	n->next=l;
	l->prev=n;
}
inline void list_remove(list *n)
{
	n->prev->next=n->next;
	n->next->prev=n->prev;
	n->next=n->prev=n;
}
inline int list_len(list *n)
{
	int len=0;
	list *p=n;
	while(p->next!=n)
	{
		p=p->next;
		len++;
	}
	return len;
}
inline int list_isempty(list *n)//返回1为空
{
	return (n->next==n);
}


#define rt_container_of(ptr, type, member) \
    ((type *)((char *)(ptr) - (unsigned long)(&((type *)0)->member)))

extern device *find_device(char *name);
extern int register_device(device *dev);	
extern int delete_device(device *dev);
extern int Get_device_num(void);
	
extern int device_init(device *dev);
extern int device_open(device *dev);	
extern int device_close(device *dev);	
extern int device_contorl(device *dev,uint8_t cmd,void *args);
extern int device_read(device *dev,int offest,uint8_t *buf,int len);	
extern int device_write(device *dev,int offest,uint8_t *buf,int len);	
	
extern int list_dev_init(void);
	

	
extern void device_lock(device *dev);	
extern void device_unlock(device *dev);		
	
typedef enum
{
	TASK_SUCCESS,
	TASK_FAILED,
	TASK_BUSY,
}TASK_STATE;
#endif
extern void auto_init(void);	
extern int device_AutoInit(void);	
#endif











