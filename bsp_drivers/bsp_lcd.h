#ifndef __BSP_LCD_H
#define __BSP_LCD_H
#include "stm32f4xx_hal.h"
#include "device.h"
#include "bsp_touch.h"
typedef struct {
	uint16_t	LCD_CMD;
	uint16_t	LCD_DATA;
}LCD_TypeDef;
//A16作为RS命令/数据区分线
#define LCD_BASE	(0x6C000000|0x0000007E)
#define LCD			((LCD_TypeDef*)LCD_BASE)

typedef struct Addr_zi{
	uint32_t ascii_12_addr;
	uint32_t ascii_16_addr;
	uint32_t ascii_24_addr;
	uint32_t ascii_32_addr;
	uint32_t GBK_12_addr;
	uint32_t GBK_16_addr;
	uint32_t GBK_24_addr;
	uint32_t size_zi;
}addr_hz;
extern _m_tp_dev tp_dev;
extern addr_hz show_zi_addr;
typedef struct  
{		 	 
	uint16_t width;
	uint16_t height;
	uint16_t id;
	uint16_t dir;
	_m_tp_dev *tp_dev;
	addr_hz  *show_zi_addr;
}LCD_DEV;

extern LCD_DEV lcd_dev;
//扫描方向定义
#define L2R_U2D  0 		//从左到右,从上到下
#define L2R_D2U  1 		//从左到右,从下到上
#define R2L_U2D  2 		//从右到左,从上到下
#define R2L_D2U  3 		//从右到左,从下到上

#define U2D_L2R  4 		//从上到下,从左到右
#define U2D_R2L  5 		//从上到下,从右到左
#define D2U_L2R  6 		//从下到上,从左到右
#define D2U_R2L  7		//从下到上,从右到左

#define DFT_SCAN_DIR  L2R_U2D  //默认的扫描方向
//#define BGRTORGB(R,G,B)	(R>>11)<<11|(G>>5)<<

//画笔颜色
#define WHITE         	 0xFFFF
#define BLACK         	 0x0000	  
#define BLUE         	 0x001F  
#define BRED             0XF81F
#define GRED 			 0XFFE0
#define GBLUE			 0X07FF
#define RED           	 0xF800
#define MAGENTA       	 0xF81F
#define GREEN         	 0x07E0
#define CYAN          	 0x7FFF
#define YELLOW        	 0xFFE0
#define BROWN 			 0XBC40 //棕色
#define BRRED 			 0XFC07 //棕红色
#define GRAY  			 0X8430 //灰色
//GUI颜色

#define DARKBLUE      	 0X01CF	//深蓝色
#define LIGHTBLUE      	 0X7D7C	//浅蓝色  
#define GRAYBLUE       	 0X5458 //灰蓝色
//以上三色为PANEL的颜色 
 
#define LIGHTGREEN     	 0X841F //浅绿色
//#define LIGHTGRAY        0XEF5B //浅灰色(PANNEL)
#define LGRAY 			 0XC618 //浅灰色(PANNEL),窗体背景色

#define LGRAYBLUE        0XA651 //浅灰蓝色(中间层颜色)
#define LBBLUE           0X2B12 //浅棕蓝色(选择条目的反色)


extern void LCD_Write_REG(uint16_t val);
extern void LCD_Write_Data(uint16_t val);
extern uint16_t LCD_Read_Data(void);
extern void LCD_WriteReg(uint16_t LCD_Reg,uint16_t LCD_RegValue);
extern uint16_t LCD_ReadReg(uint16_t LCD_Reg);
extern void LCD_SetCursor(uint16_t x,uint16_t y);
extern uint16_t Read_Point_Color(uint16_t x,uint16_t y);
extern void LCD_Display_On(void);
extern void LCD_Display_Off(void);
extern void LCD_Scan_Dir(void);
extern void LCD_DrawPoint(uint16_t x,uint16_t y,uint16_t color);
extern void LCD_SSD_BackLightSet(uint8_t pwm);
extern void LCD_Display_Dir(uint8_t dir);
extern void LCD_Set_Window(uint16_t sx,uint16_t sy,uint16_t width,uint16_t height);
extern void LCD_Clear(uint16_t color);
extern int LCD_Init(void);
extern int LCD_DeInit(void);
extern void LCD_Fill(uint16_t x1,uint16_t y1,uint16_t x2,uint16_t y2,uint16_t color);
extern void LCD_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,uint16_t color);
extern void LCD_DrawRectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,uint16_t color);
extern void LCD_Draw_Circle(uint16_t x0,uint16_t y0,uint8_t r,uint16_t color);
extern void LCD_ShowChar(uint16_t x,uint16_t y,uint8_t num,uint8_t size,uint16_t color);
extern void LCD_ShowNum(uint16_t x,uint16_t y,uint32_t num,uint8_t len,uint8_t size,uint16_t color);
extern void LCD_ShowxNum(uint16_t x,uint16_t y,uint32_t num,uint8_t len,uint8_t size,uint16_t color);
extern void LCD_ShowString(uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t size,uint8_t *p,uint16_t color);
extern void LCD_Color_Fill(uint16_t sx,uint16_t sy,uint16_t ex,uint16_t ey,uint16_t *color);

extern void LCD_ShowAll(uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t size,uint8_t *p,uint16_t color);
extern void LCD_ShowChineseString(uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t size,uint8_t *p,uint16_t color);
extern uint8_t update_flashfont(void);
#endif




