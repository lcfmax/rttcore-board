#include "soft_spi.h"
#include "config.h"
#define DOUT 		PBin(2)   	//T_MISO
#define TDIN 		PFout(11)  	//T_MOSI
#define TCLK 		PBout(0)  	//T_TCLK
#define TCS  		PCout(13)  	//T_CS  

//https://www.cnblogs.com/1024E/p/13322892.html
//http://www.openedv.com/posts/list/39016.htm
//首先定义好I/O口CPOL=1，CPHA=1
//uint8_t SOFT_SPI_RW(uint8_t byte) 
//{ 
//	uint8_t i,Temp=0; 
//	for(i=0;i<8;i++)    	// 循环8次 
//	{ 
//		TCLK = 0;     		//拉低时钟 
//		if(byte&0x80) 
//			TDIN = 1;		//若最到位为高，则输出高 
//		else       
//			TDIN = 0; 		//若最到位为低，则输出低 
//		byte <<= 1;     	// 低一位移位到最高位 
//		TCLK = 1;     		//拉高时钟 
//		Temp <<= 1;     	//数据左移 
//		if(DOUT) Temp++;    //若从从机接收到高电平，数据自加一
//		TCLK = 0;     		//拉低时钟 
//	} 
//	return (Temp);     		//返回数据 
//}
uint8_t SOFT_SPI_RW(uint8_t byte)//CPOL=0，CPHA=1
{
	uint8_t i,Temp=0;
	TCLK = 0;
	for(i=0;i<8;i++)     	// 循环8次
	{
		TCLK = 1;     		//拉高时钟
		if(byte&0x80)
			TDIN = 1;  		//若最到位为高，则输出高
		else      
			TDIN = 0;   	//若最到位为低，则输出低
		byte <<= 1;     	// 低一位移位到最高位
		TCLK = 0;     		//拉低时钟
		Temp <<= 1;    	 	//数据左移
		
		if(DOUT)
			Temp++;     	//若从从机接收到高电平，数据自加一
		TCLK = 1;     		//拉高时钟
	}
	return (Temp);     		//返回数据
}
void xpt_2046_init(void)
{
	TCS=0;
	SOFT_SPI_RW(0x80);
	SOFT_SPI_RW(0x00);
	SOFT_SPI_RW(0x00);
	delay_us(1);
	TCS=1;
}
uint16_t SPI_Send(uint8_t data)
{
	uint16_t xy=0;
	TCS=0;
	SOFT_SPI_RW(data);
	xy=SOFT_SPI_RW(0);
	xy<<=8;
	xy|=SOFT_SPI_RW(0);
	xy>>=4;
	TCS=1;
	return xy;
}








