#ifndef __BSP_SPIFLASH_H
#define __BSP_SPIFLASH_H
#include "stm32f4xx_hal.h"
#include "config.h"
#define   SPI_CAPCITY		128/8*1024*1024
#define READ_FLASH_ID 		0x90
#define WRITE_ENABLE  		0x06
#define WRITE_DISABLE 		0x04
#define READ_STATUS_REG1 	0x05
#define READ_STATUS_REG2 	0x35
#define WRITE_STATUS_REG 	0x01
#define PAGE_POGRAM 		0x02
#define SECTOR_ERASE_4K 	0x20
/*The Sector Erase instruction sets all memory within a specified sector (4K-bytes) to the erased state of all
1s (FFh). A Write Enable instruction must be executed before the device will accept the Sector Erase
Instruction (Status Register bit WEL must equal 1)*/
#define CHIP_ERASE 			0xc7
#define READ_DATA			0x03
#define POWERDOWN			0xB9 
#define REALEASEPOWERDOWN	0xAB 
enum{
 W25Q_READ_FLASH_ID,
 W25Q_WRITE_ENABLE ,
 W25Q_WRITE_DISABLE,
 W25Q_READ_STATUS_REG1,
 W25Q_READ_STATUS_REG2,
 W25Q_WRITE_STATUS_REG1,
 W25Q_WRITE_STATUS_REG2,
 W25Q_PAGE_POGRAM,
 W25Q_SECTOR_ERASE_4K,
 W25Q_CHIP_ERASE,
 W25Q_READ_DATA,
 W25Q_POWERDOWN, 
 W25Q_REALEASEPOWERDOWN,
 W25Q_READY,	
};
extern void SPI_sector_erased(uint32_t sector_addr);
extern void spi_write_enable(void);
extern void spi_write_disable(void);
extern uint16_t spi_read_id(void);
extern uint8_t spi_read_status(uint8_t index);
extern void spi_write_status(uint8_t index,uint8_t sr);
extern uint8_t Get_SPI_BUSY(void);
extern void wait_busy(void);
extern void SPI_sector_erased(uint32_t sector_addr);
extern void SPI_chip_erased(void);
extern void write_page_program(int addr,uint8_t *buf,int len);
extern void SPI_Write_NoCheck(int addr,uint8_t *buf,int len);
extern void Power_down(void);
extern void wakeup(void);
#endif




