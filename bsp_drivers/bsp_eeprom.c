#include "bsp_eeprom.h"
#include "bsp_i2c.h"
#define device  struct rt_device
device AT24C20;
rt_err_t AT24C20_init(device *dev)
{
	i2c_init();
	return RT_EOK;
}
rt_size_t AT24C02_Read(device *dev,rt_off_t offest,void *buf,rt_size_t len)
{
	 
	if(offest+len>0xff) return false;
	uint8_t *point=(uint8_t*)buf;
	while(len--)
	{
		*point=IIC_readbyte(0xA0,offest++);
		point++;
	}
	 
	return true;
}
rt_size_t AT24C02_Write(device *dev,rt_off_t offest,const void *buf,rt_size_t len)
{
	if(len+offest>255)
	{
		return false;
	}
	 uint8_t *point=(uint8_t*)buf;
	while(len--)
	{
		IIC_writebyte(0xA0,(uint16_t)offest,(*point));
		offest++;
		point++;
	}
	 
	return true;
}

int i2c_register_device()
{
	AT24C20.init=AT24C20_init;
	AT24C20.read=AT24C02_Read;
	AT24C20.write=AT24C02_Write;
	return rt_device_register(&AT24C20,"at24c02",RT_DEVICE_FLAG_RDWR);
}

DEVICE_INIT(i2c_register_device);




















