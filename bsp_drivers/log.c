#include "log.h"
#include "rtthread.h"

#define FLASH_LOG_START_ADDR		(0x08000000+512*1024)
#define FLASH_LOG_SIZE				(128*1024)
#define FLASH_LOG_END_ADDR			(FLASH_LOG_START_ADDR+FLASH_LOG_SIZE)		
#define FLASH_LOG_HEADER			0xA55AA55A
__packed struct FLASH_STRUCTURE{
	uint32_t header;
	uint32_t flash_log_next_addr;
	uint32_t flash_log_using_size;	
};
typedef struct FLASH_STRUCTURE flash_log;
__packed struct FLASH_STRUCTURE flash_log_default={
	FLASH_LOG_HEADER,
	FLASH_LOG_START_ADDR,
	0xffffffff,
};
__packed struct FLASH_STRUCTURE flash_log_structure={
	FLASH_LOG_HEADER,
	FLASH_LOG_START_ADDR,
	0,
};
static int flash_log_format(void);
int flash_log_init()
{
	uint32_t flash_head=0;
	uint32_t addr=0;
	if(stm32_flash_read(flash_log_default.flash_log_next_addr,(uint8_t*)&flash_log_default,sizeof(flash_log_default))!=RT_EOK)
		return RT_ERROR;
	if(flash_head!=FLASH_LOG_HEADER)
	{
		return flash_log_format();
	}
	else 
	{
		addr=find_next_log_addr();
		if(addr!=0xffffffff)
			flash_log_structure.flash_log_next_addr=addr;
		else
			return RT_ERROR;
	}
		return RT_EOK;
}
static int flash_log_format()
{
	if(stm32_flash_erase(FLASH_LOG_START_ADDR,FLASH_LOG_SIZE)!=RT_EOK)
		return RT_ERROR;
	if(stm32_flash_write(FLASH_LOG_START_ADDR,(uint8_t*)&flash_log_default,sizeof(flash_log_default))!=RT_EOK)
		return RT_ERROR;
	return RT_EOK;
}
uint32_t find_next_log_addr()
{
	flash_log flash_log_find={0};
	uint32_t flash_addr=0;
	if(stm32_flash_read(flash_log_default.flash_log_next_addr,(uint8_t*)&flash_log_find,sizeof(flash_log_find))!=RT_EOK)
		return 0xffffffff;
	if(flash_log_find.flash_log_next_addr==flash_log_default.flash_log_next_addr&&flash_log_find.header==FLASH_LOG_HEADER)
	{
		if(flash_log_find.flash_log_using_size==0xffffffff)
			return flash_log_find.flash_log_next_addr;
		else
		{
			while(1)
			{
				flash_addr=FLASH_LOG_START_ADDR+flash_log_find.flash_log_using_size;
				if(stm32_flash_read(flash_addr,(uint8_t*)&flash_log_find,sizeof(flash_log_find))!=RT_EOK)
					return RT_ERROR;
				if(flash_log_find.flash_log_next_addr!=flash_addr||flash_log_find.header!=FLASH_LOG_HEADER)
					return flash_log_find.flash_log_next_addr;//返回下一条记录的起始地址
			}		
		}
	}
	else 
		return 0xffffffff;
}
int Set_Log(char *log,size_t len)
{
	if(flash_log_structure.header!=FLASH_LOG_HEADER)
		return RT_ERROR;
	if(flash_log_structure.flash_log_next_addr>=FLASH_LOG_END_ADDR||flash_log_structure.flash_log_next_addr+len>FLASH_LOG_END_ADDR)
	{
		rt_memcpy(&flash_log_structure,&flash_log_default,4);
		flash_log_format();		
	}		
	flash_log_structure.flash_log_using_size+=len;
	if(stm32_flash_write(flash_log_structure.flash_log_next_addr,(uint8_t*)&flash_log_structure,sizeof(flash_log_structure))!=RT_EOK)
		return RT_ERROR;
	flash_log_structure.flash_log_next_addr+=sizeof(flash_log_structure);
	if(stm32_flash_write(flash_log_structure.flash_log_next_addr,(uint8_t*)log,len)!=RT_EOK)
		return RT_ERROR;
	flash_log_structure.flash_log_next_addr+=len;
	return RT_EOK;
}



















