#ifndef __BSP_RTC_H
#define __BSP_RTC_H
#include "stdint.h"
typedef struct{
    uint8_t year;
    uint8_t month;
    uint8_t date;
    uint8_t hour;
    uint8_t minute;
    uint8_t seconds;
}RTC_time; 

extern void Get_Alarm(RTC_time *time);
extern void Set_Alarm(RTC_time *time);
extern void Get_time(RTC_time *time);
extern void Set_time(RTC_time *time);






#endif






