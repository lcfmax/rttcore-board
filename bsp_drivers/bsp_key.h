#ifndef __BSP_KEY_H
#define __BSP_KEY_H

#include "stm32f4xx_hal.h"

enum{
	KEY0,
	WE_UP,
};

extern uint8_t Get_Key_Value(uint8_t key_index);

#endif





