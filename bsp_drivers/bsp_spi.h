#ifndef __BSP_SPI_H
#define __BSP_SPI_H
#include "stm32f4xx_hal.h"


extern int MX_SPI1_Init(void);
extern void Set_SPI1_Speed(uint8_t speed );
extern uint8_t SPI1_RW_Byte(uint8_t tx_byte);

#endif




