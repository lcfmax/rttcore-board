#include "tool.h"
#include "string.h"
#include "stdbool.h"
#include "stdlib.h"
#include "stm32f4xx_ll_rcc.h"
uint8_t Get_value_by_key_MQTT(char *root_str,int Inlen,char *key_t,char *value_str,float *value_num,uint8_t *type,int *len,char *Set_Par_value)
{
	int i=0,m=0;
	char temp[30]={0};
	int j=KMP(root_str,Inlen,key_t,strlen(key_t));
	if(j!=-1)
	{
		for(i=j;i<Inlen;i++)
		{
			if(root_str[i]==',')
			{
				if(root_str[j+2]!='"')
				{
					memcpy(temp,&root_str[j+2],i-j+1-3);
					*value_num=atof(temp);
					*type=0;//数字
					*len=i-j+1-3;
				}
				else
				{
					memcpy(temp,&root_str[j+3],i-j+1-5);
					memcpy(value_str,temp,i-j+1-5);
					*type=1;//字符
					*len=i-j+1-5;
				}
				memcpy(Set_Par_value,temp,sizeof(temp));
				return true;
			}
			else if(root_str[i]=='}')
			{
				if(root_str[j+2]!='"')
				{
					for(m=j+2;m<Inlen;m++)
					{
						if(root_str[m]>39||root_str[m]<30)
						{
							memcpy(temp,&root_str[j+2],m-j+1-3);
							*value_num=atof(temp);
							*type=0;//数字
							*len=i-j+1-3;
							memcpy(Set_Par_value,temp,sizeof(temp));
							return true;
						}
					}
				}
				else
				{
					for(m=j+3;m<Inlen;m++)
					{
						if(root_str[m]=='"')
						{
							memcpy(temp,&root_str[j+3],m-j+1-4);
							memcpy(value_str,temp,m-j+1-4);
							*type=1;//字符
							*len=m-j+1-4;
							memcpy(Set_Par_value,temp,sizeof(temp));
							return true;
						}
					}
				}	
			}
		}
		return true;
	}
	*len=0;
	*type=255;
	return false;	
}
uint8_t Get_value_by_key(char *root_str,int Inlen,char *key_t,char *value_str,float *value_num,uint8_t *type,int *len)
{
	int i=0;
	char temp[30]={0};
	int j=KMP(root_str,Inlen,key_t,strlen(key_t));
	if(j!=-1)
	{
		if(root_str[j+1]=='"')
		{
			for(i=j+2;i<Inlen;i++)
			{
				if(root_str[i]=='"')
					break;
			}
			memcpy(value_str,&root_str[j+2],i-j-2);
			*type=1;//字符
			*len=i-j-2-1;
		}
		else
		{
			for(i=j+1;i<Inlen;i++)
			{
				if(root_str[i]=='\r')
					break;
			}
			memcpy(temp,&root_str[j+1],i-j);
			*value_num=atof(temp);
			*type=0;//数字
			*len=i-j-1;
		}
		return true;
	}
	*len=0;
	*type=255;
	return false;	
}
/**************************************************************************************************
 * @fn          Getnext
 * @brief       返回查询字符串的next数组
 * @param    	next数组，匹配字符串，匹配字符串的长度
 * @return      1-成功，0-失败
 **************************************************************************************************/
void Getnext(int *next,char *t,int len)
{
   int j=0,k=-1;
   next[0]=-1;
   while(j<len)
   {
      if(k == -1 || t[j] == t[k])
      {
         j++;
		 k++;
         next[j] = k;
      }
      else k = next[k];
   }
}
/**************************************************************************************************
 * @fn          KMP
 * @brief       查找匹配字符串的位置 
 * @param    	s-被查找字符串，s_len-被查找字符串长度，t-查找字符串，t_len-查找字符串长度
 * @return      -1-查找失败 其他值-子串的最后位置的后一个字节的位置
 **************************************************************************************************/
int KMP(char *s,int s_len,char *t,int t_len)
{
   int next[30]={0},i=0,j=0;

   Getnext(next,t,t_len);
   while(i<s_len&&j<t_len)
   {
      if(j==-1 || s[i]==t[j])
      {
         i++;
         j++;
      }
      else j=next[j];               //j回退。。。
   }
   if(j>=t_len)
       return i;         //匹配成功，返回子串的最后位置的后一个字节
   else
      return (-1);                  //没找到
}

/**************************************************************************************************
 * @fn       is_SFTRSTF
 * @brief    返回是否是软件重启
 * @param    无
 * @return   1-是软件重启，0-不是
 **************************************************************************************************/
uint8_t IS_SFTRSTF(void)
{
	return LL_RCC_IsActiveFlag_SFTRST();
}

__asm void MSR_MSP(uint32_t addr) 
{
	MSR MSP, r0 			//set Main Stack value
	BX r14
}






