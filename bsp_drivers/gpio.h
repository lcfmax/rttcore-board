#ifndef _GPIO_H
#define _GPIO_H

//把"位带地址＋位序号"转换成别名地址的宏

#define BITBAND(addr,bitnum) ((addr & 0xF0000000)+0x2000000+ ( (addr &0xFFFFF)<<5)+(bitnum<<2))

//把该地址转换成一个指针
#define MEM_ADDR(addr) * ( (volatile unsigned long * ) (addr) )
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 

#define GPIO_ADDR(addr,bitnum)		* ( (volatile unsigned long * ) ((addr & 0xF0000000)+0x2000000+ ( (addr &0xFFFFF)*32)+(bitnum*4)) )
	
#define GPIOA_ODR_Addr    (GPIOA_BASE+20) //0x40020014
#define GPIOB_ODR_Addr    (GPIOB_BASE+20) //0x40020414 
#define GPIOC_ODR_Addr    (GPIOC_BASE+20) //0x40020814 
#define GPIOD_ODR_Addr    (GPIOD_BASE+20) //0x40020C14 
#define GPIOE_ODR_Addr    (GPIOE_BASE+20) //0x40021014 
#define GPIOF_ODR_Addr    (GPIOF_BASE+20) //0x40021414    
#define GPIOG_ODR_Addr    (GPIOG_BASE+20) //0x40021814   
#define GPIOH_ODR_Addr    (GPIOH_BASE+20) //0x40021C14    
#define GPIOI_ODR_Addr    (GPIOI_BASE+20) //0x40022014 
#define GPIOJ_ODR_ADDr    (GPIOJ_BASE+20) //0x40022414
#define GPIOK_ODR_ADDr    (GPIOK_BASE+20) //0x40022814

#define GPIOA_IDR_Addr    (GPIOA_BASE+16) //0x40020010 
#define GPIOB_IDR_Addr    (GPIOB_BASE+16) //0x40020410 
#define GPIOC_IDR_Addr    (GPIOC_BASE+16) //0x40020810 
#define GPIOD_IDR_Addr    (GPIOD_BASE+16) //0x40020C10 
#define GPIOE_IDR_Addr    (GPIOE_BASE+16) //0x40021010 
#define GPIOF_IDR_Addr    (GPIOF_BASE+16) //0x40021410 
#define GPIOG_IDR_Addr    (GPIOG_BASE+16) //0x40021810 
#define GPIOH_IDR_Addr    (GPIOH_BASE+16) //0x40021C10 
#define GPIOI_IDR_Addr    (GPIOI_BASE+16) //0x40022010 
#define GPIOJ_IDR_Addr    (GPIOJ_BASE+16) //0x40022410 
#define GPIOK_IDR_Addr    (GPIOK_BASE+16) //0x40022810 

#define GPIOA_MODE_Addr    (GPIOA_BASE) //0x40020000
#define GPIOB_MODE_Addr    (GPIOB_BASE) //0x40020400 
#define GPIOC_MODE_Addr    (GPIOC_BASE) //0x40020800 
#define GPIOD_MODE_Addr    (GPIOD_BASE) //0x40020C00 
#define GPIOE_MODE_Addr    (GPIOE_BASE) //0x40021000 
#define GPIOF_MODE_Addr    (GPIOF_BASE) //0x40021400    
#define GPIOG_MODE_Addr    (GPIOG_BASE) //0x40021800  
#define GPIOH_MODE_Addr    (GPIOH_BASE) //0x40021C00    
#define GPIOI_MODE_Addr    (GPIOI_BASE) //0x40022000 
#define GPIOJ_MODE_ADDr    (GPIOJ_BASE) //0x40022400
#define GPIOK_MODE_ADDr    (GPIOK_BASE) //0x40022800

#define 	GPIO_INPUT      0
#define 	GPIO_OUTPUT		1
#define 	GPIO_AF			2
#define 	GPIO_SIMULATION	3


#define GPIO_BASE(name)     GPIO##name##_BASE
#define GPIO_ODR_NAME(name)     GPIO##name##_ODR_Addr
#define GPIO_IDR_NAME(name)     GPIO##name##_IDR_Addr
#define PIN_MODE(name,n,mode)		do{\
	if(n==0){	GPIO_ADDR(GPIO_BASE(name),n)=mode&0x01;\
				GPIO_ADDR(GPIO_BASE(name),n+1)=((mode>>1)&0x01);}\
	else {\
				GPIO_ADDR(GPIO_BASE(name),n*2)=mode&0x01;\
				GPIO_ADDR(GPIO_BASE(name),n*2+1)=(mode>>1)&0x01;\
	}\
}while(0)
#define PIN_OUT(port,n) GPIO_ADDR(GPIO_ODR_NAME(port),n)
#define PIN_IN(port,n) GPIO_ADDR(GPIO_IDR_NAME(port),n)
//IO口操作,只对单一的IO口!
//确保n的值小于16!
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入 

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出 
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入 

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出 
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入 

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出 
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入 

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出 
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //输出 
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //输入

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入

#define PHout(n)   BIT_ADDR(GPIOH_ODR_Addr,n)  //输出 
#define PHin(n)    BIT_ADDR(GPIOH_IDR_Addr,n)  //输入

#define PIout(n)   BIT_ADDR(GPIOI_ODR_Addr,n)  //输出 
#define PIin(n)    BIT_ADDR(GPIOI_IDR_Addr,n)  //输入

#define PJout(n)   BIT_ADDR(GPIOJ_ODR_Addr,n)  //输出 
#define PJin(n)    BIT_ADDR(GPIOJ_IDR_Addr,n)  //输入

#define PKout(n)   BIT_ADDR(GPIOK_ODR_Addr,n)  //输出 
#define PKin(n)    BIT_ADDR(GPIOK_IDR_Addr,n)  //输入



#endif






