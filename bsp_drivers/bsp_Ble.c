#include "bsp_Ble.h"
#include "tool.h"
#include "Dev_usart.h"


BLE_TABLE ble_table[]={
	{"+++a\r\n",					"a+ok",	"ERROR",500,3},
	{"AT\r\n",					"OK",	"ERROR",500,3},
	{"AT+NAME=yhj\r\n",			"OK",	"ERROR",500,3},
	{"AT+HELLO=ATK-BLE01\r\n",	"OK",	"ERROR",500,3},
	{"AT+TPL=6\r\n",			"OK",	"ERROR",500,3},
	{"AT+UART=115200,8,1,0\r\n","OK",	"ERROR",500,3},
	{"AT+ADPTIM=5\r\n",			"OK",	"ERROR",500,3},
	{"AT+LINKPASSEN=ON\r\n",	"OK",	"ERROR",500,3},
	{"AT+LINKPASS=123456\r\n",	"OK",	"ERROR",500,3},
	{"AT+LEDEN=ON\r\n",			"OK",	"ERROR",500,3},
	{"AT+SLAVESLEEPEN=OFF\r\n",	"OK",	"ERROR",500,3},
	{"AT+MAXPUT=OFF\r\n",		"OK",	"ERROR",500,3},
	{"AT+MODE=s\r\n",			"OK",	"ERROR",500,3},
	{"完成\r\n",				"OK",	"ERROR",500,3},
};
#define BLE_SIZE (sizeof(ble_table)/sizeof(BLE_TABLE))

DEV_BLE Dev_Ble;
static void wait_cmd_ack(void);
static void Ble_recvData_handle(void);
void Ble_timeout(void *parameter)
{
	*(uint8_t*)parameter=1;
    // 
}
static void Ble_send_entry(void *parameter)
{
    while(1)
    {
		if(rt_sem_take(Dev_Ble.tx_sem,RT_WAITING_FOREVER)==RT_EOK)
		{
			Uart_Write(Dev_Ble.uart_dev,Dev_Ble.tx_data_point,Dev_Ble.tx_data_len);
		}
		rt_thread_delay(3);
    }
}
static void Ble_recv_entry(void *parameter)
{
 
    while (1)
    {
       switch (Dev_Ble.step)
       {
        case 0:
            if(Uart_Write(Dev_Ble.uart_dev,(uint8_t*)Dev_Ble.Ble_table[Dev_Ble.AT_index].cmd,
                            strlen(Dev_Ble.Ble_table[Dev_Ble.AT_index].cmd))==RT_EOK)
                Dev_Ble.step=1;
            else
                Dev_Ble.step=3;
             break;
        case 1:
             wait_cmd_ack();
             break;
        case 2:
			 Ble_recvData_handle();
             break;
        case 3:
             rt_thread_suspend(Dev_Ble.Ble_rx_tid);
             break;
       default:
           break;
       }
        rt_thread_delay(3);
    }
}

static void wait_cmd_ack(void)
{
    uint8_t flag=0;
    uint16_t rx_len=0;   
    flag=Uart_Get_Rx_State(Dev_Ble.uart_dev,&rx_len);
    if(flag==0&&rx_len!=0)
        rt_timer_start(Dev_Ble.Ble_timeOut_timer);  
    else if(flag==1&&rx_len!=0)
    {
        if(Dev_Ble.timeout_flag!=1)
			return;
        Dev_Ble.read_buf=my_alloc(rx_len+1);
        Uart_Read(Dev_Ble.uart_dev,Dev_Ble.read_buf,rx_len);
        if(KMP((char*)Dev_Ble.read_buf,rx_len,Dev_Ble.Ble_table[Dev_Ble.AT_index].true_ack,
                    strlen(Dev_Ble.Ble_table[Dev_Ble.AT_index].true_ack))!=-1)
                    {
						
                        my_free(Dev_Ble.read_buf);
                        if(Dev_Ble.AT_index==BLE_SIZE-1)
                        {
                            for(int i=0;i<BLE_SIZE;i++)
                                Dev_Ble.Ble_table[i].retry_times=3;
                            Dev_Ble.step=2;
                            Dev_Ble.AT_index=0;
							Dev_Ble.timeout_flag=0;
                            return;
                        }
                        Dev_Ble.step=0;                            
                        Dev_Ble.AT_index++;
                    }
        else
        {
            my_free(Dev_Ble.read_buf);
            if(Dev_Ble.Ble_table[Dev_Ble.AT_index].retry_times==0)
            {
                Dev_Ble.step=3;
                for(int i=0;i<BLE_SIZE;i++)
                    Dev_Ble.Ble_table[i].retry_times=3;
				Dev_Ble.timeout_flag=0;
                return;
            }
            Dev_Ble.Ble_table[Dev_Ble.AT_index].retry_times--;
            Dev_Ble.step=0; 
			Dev_Ble.timeout_flag=0;
        }     
    } 
}
static void recv_data_handle(char *recv_data,int recv_len)
{
   
}
static void Ble_recvData_handle(void)
{
	uint8_t flag=0;
    uint16_t rx_len=0;
	flag=Uart_Get_Rx_State(Dev_Ble.uart_dev,&rx_len);
    if(flag==0&&rx_len!=0)
        rt_timer_start(Dev_Ble.Ble_timeOut_timer);  
    else if(flag==1&&rx_len!=0)
    {
        rt_thread_mdelay(10);
        Dev_Ble.read_buf=my_alloc(rx_len+1);
		Uart_Read(Dev_Ble.uart_dev,Dev_Ble.read_buf,rx_len);
		rt_kprintf("接收到数据字节数%d,内容为%s\r\n",rx_len,Dev_Ble.read_buf);
		recv_data_handle((char*)Dev_Ble.read_buf,rx_len);
		my_free(Dev_Ble.read_buf);
	}
}
rt_err_t BLE_Init(rt_device_t dev)
{
	Dev_Uart_Init(2);
    Dev_Ble.uart_dev=rt_device_find("uart2");
	
    if(Dev_Ble.uart_dev==NULL)
    {
        rt_kprintf("uart2查找失败\r\n");
        return RT_ERROR;
    }
    Dev_Ble.Ble_table=ble_table;
    Dev_Ble.Ble_rx_tid=rt_thread_create("Ble_rx",Ble_recv_entry,RT_NULL,512,5,10);
    Dev_Ble.Ble_tx_tid=rt_thread_create("Ble_tx",Ble_send_entry,RT_NULL,512,5,10);
    if(Dev_Ble.Ble_rx_tid==RT_NULL||Dev_Ble.Ble_tx_tid==RT_NULL)
    {
        rt_kprintf("Ble线程创建失败\r\n");
        return RT_ERROR;
    }
    Dev_Ble.tx_sem=rt_sem_create("Ble_sem",0,RT_IPC_FLAG_FIFO);
	Dev_Ble.read_sem=rt_sem_create("readwfsem",0,RT_IPC_FLAG_FIFO);
    Dev_Ble.Ble_timeOut_timer=rt_timer_create("Ble_timer",Ble_timeout,
														&Dev_Ble.timeout_flag,Dev_Ble.Ble_table[0].timeout,
														RT_TIMER_FLAG_ONE_SHOT | RT_TIMER_FLAG_SOFT_TIMER);
    if(Dev_Ble.Ble_timeOut_timer==RT_NULL)
    {
        rt_kprintf("Ble定时器创建失败\r\n");
        return RT_ERROR;
    }
    rt_thread_startup(Dev_Ble.Ble_rx_tid);   
    rt_thread_startup(Dev_Ble.Ble_tx_tid);                                          
    return RT_EOK;
}
int Ble_Get_Rx_Len(int connect_device_offest)
{
	return Dev_Ble.Ble_recv_data[connect_device_offest].recv_len;
}
rt_size_t Ble_Read(rt_device_t Ble_dev,rt_off_t connect_device_offest,void *read_buf,rt_size_t read_len)
{
	if(rt_sem_take(Dev_Ble.read_sem,RT_WAITING_FOREVER)==RT_EOK)
	{
		if(Dev_Ble.Ble_recv_data[connect_device_offest].recv_len<read_len)
		read_len=Dev_Ble.Ble_recv_data[connect_device_offest].recv_len;
		return rt_ringbuffer_get(Dev_Ble.Ble_recv_data[connect_device_offest].rx_ringbuffer,(uint8_t*)read_buf,read_len);
	}
	else 
		return 0;
}
rt_size_t Ble_Write(rt_device_t Ble_dev,rt_off_t connect_device_offest,const void *write_buf,rt_size_t write_len)
{
	char send_data[30]= {0};
	sprintf(send_data,"AT+CIPSEND=%d,%d\r\n",(int)connect_device_offest,(int)write_len);
	Uart_Write(Dev_Ble.uart_dev,(uint8_t*)send_data,strlen(send_data));
	Dev_Ble.tx_data_point=(uint8_t*)write_buf;
	Dev_Ble.tx_data_len=write_len;
	return write_len;
}
rt_err_t Ble_control(rt_device_t dev,int cmd,void *args)
{
    switch (cmd&0X0F)
    {
		case GET_BLE_RX_LEN:
			*(int*)args=Ble_Get_Rx_Len((cmd>>4)&0X0F);
			break;
		case QUERY_CONNECT_IP_PORT:
			break;
		default:
			break;
    }
    return RT_EOK;
}
int Ble_register()
{
    Dev_Ble.dev.init=BLE_Init;
    Dev_Ble.dev.read=Ble_Read;
    Dev_Ble.dev.write=Ble_Write;
    Dev_Ble.dev.control=Ble_control;
	rt_device_register(&Dev_Ble.dev,"ble",RT_DEVICE_OFLAG_RDWR);
	return RT_EOK;
}
APP_INIT(Ble_register);
uint8_t Bletx_buf[30]={0};
void Ble_send_data(uint8_t argc, char **argv)
{
    rt_device_t Ble_dev=rt_device_find("ble");
    if(Ble_dev==NULL) return;
	rt_memset(Bletx_buf,0,30);
	rt_memcpy(Bletx_buf,argv[2],strlen(argv[2]));
    Ble_Write(Ble_dev,atoi(argv[1]),Bletx_buf,strlen(argv[2]));
}
MSH_CMD_EXPORT(Ble_send_data,Ble SEND DATA);





