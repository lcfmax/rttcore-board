#include "bsp_i2c.h"
#include "config.h"
#define SCL_HIGH 	PIN_OUT(B,8)=1
#define SCL_LOW 	PIN_OUT(B,8)=0
#define SDA_HIGH 	PIN_OUT(B,9)=1
#define SDA_LOW 	PIN_OUT(B,9)=0

#define SDA_OUT		PIN_MODE(B,9,GPIO_OUTPUT)
#define SDA_IN		PIN_MODE(B,9,GPIO_INPUT)

#define READ_SDA    PIN_IN(B,9)
#define WRITE_SDA	PIN_OUT(B,9)
int i2c_init()
{
	GPIO_InitTypeDef gpio_struct;
	__HAL_RCC_GPIOB_CLK_ENABLE(); //使能 GPIOB 时钟

	gpio_struct.Mode=GPIO_MODE_OUTPUT_PP;
	gpio_struct.Pin=GPIO_PIN_8|GPIO_PIN_9;
	gpio_struct.Pull=GPIO_PULLUP;
	gpio_struct.Speed=GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOB,&gpio_struct);
	SCL_HIGH;
	SDA_HIGH;
	return 1;
}

void i2c_start()
{
	SDA_OUT;
	SCL_HIGH;
	SDA_HIGH;
	delay_us(4);
	SDA_LOW;
	delay_us(4);
	SCL_LOW;	
}

void i2c_stop()
{
	SDA_OUT;
	SDA_LOW;
	SCL_LOW;
	delay_us(4);
	SCL_HIGH;
	delay_us(4);
	SDA_HIGH;
	delay_us(4);
}

uint8_t I2C_wait_ack()
{
	uint8_t tcnt=0;
	SDA_HIGH;
	delay_us(1);
	SDA_IN;
	SCL_HIGH;
	delay_us(1);
	SDA_IN;
	while(READ_SDA)
	{
		tcnt++;
		if(tcnt>250)
		{
			i2c_stop();
			return false;
		}
	}
	SCL_LOW;
	return true;
}

void i2c_ack()
{
	SDA_OUT;	
	SCL_LOW;
	SDA_LOW;
	delay_us(2);
	SCL_HIGH;
	delay_us(2);
	SCL_LOW;
}
void i2c_nack()
{
	SDA_OUT;	
	SCL_LOW;
	SDA_HIGH;
	delay_us(2);
	SCL_HIGH;
	delay_us(2);
	SCL_LOW;
}
void i2c_sendbyte(uint8_t buf)
{
	int t=0;
	SDA_OUT;
	SCL_LOW;
	for(t=0;t<8;t++)
	{
		WRITE_SDA=(buf&0x80)>>7;
		buf<<=1;
		delay_us(2);
		SCL_HIGH;
		delay_us(2); 
		SCL_LOW;	
		delay_us(2);
	}
}

uint8_t i2c_readbyte(uint8_t is_ack)
{
	uint8_t temp=0;
	int i=0;
	SDA_IN;
	for(i=0;i<8;i++)
	{
		SCL_LOW;
		delay_us(2);
		SCL_HIGH;
		temp<<=1;
		if(READ_SDA) temp++;
		delay_us(1);
	}
	if(is_ack==true)
		i2c_ack();
	else
		i2c_nack();
	return temp;
}

int IIC_writebyte(uint8_t dev_addr,uint16_t write_addr,uint8_t DataToWrite)
{
	i2c_start();
	i2c_sendbyte(dev_addr);
	I2C_wait_ack();
	i2c_sendbyte(write_addr&0xff);
	I2C_wait_ack();
	i2c_sendbyte(DataToWrite);
	I2C_wait_ack();
	i2c_stop();
	delay_ms(10);
	return true;
}
uint8_t IIC_readbyte(uint8_t dev_addr,uint16_t read_addr)
{
	uint8_t temp=0;
	i2c_start();
	i2c_sendbyte(dev_addr);
	I2C_wait_ack();
	i2c_sendbyte(read_addr&0xff);
	I2C_wait_ack();
	i2c_start();
	i2c_sendbyte(dev_addr|1);  //进入接收模式
	I2C_wait_ack();
	temp=i2c_readbyte(0);
	i2c_stop();
	delay_ms(10);
	return temp;
}


