#include "bsp_lcd.h"

#include "font.h"
#define 	LCD_LED 	PIN_OUT(B,15)	//背光控制，1点亮
SRAM_HandleTypeDef hsram1;
LCD_DEV lcd_dev;
void MX_FSMC_Init(void)
{

  /* USER CODE BEGIN FSMC_Init 0 */

  /* USER CODE END FSMC_Init 0 */

  FSMC_NORSRAM_TimingTypeDef Timing = {0};
  FSMC_NORSRAM_TimingTypeDef ExtTiming = {0};

  /* USER CODE BEGIN FSMC_Init 1 */

  /* USER CODE END FSMC_Init 1 */

  /** Perform the SRAM1 memory initialization sequence
  */
  hsram1.Instance = FSMC_NORSRAM_DEVICE;
  hsram1.Extended = FSMC_NORSRAM_EXTENDED_DEVICE;
  /* hsram1.Init */
  hsram1.Init.NSBank = FSMC_NORSRAM_BANK4;
  hsram1.Init.DataAddressMux = FSMC_DATA_ADDRESS_MUX_DISABLE;
  hsram1.Init.MemoryType = FSMC_MEMORY_TYPE_SRAM;
  hsram1.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_16;
  hsram1.Init.BurstAccessMode = FSMC_BURST_ACCESS_MODE_DISABLE;
  hsram1.Init.WaitSignalPolarity = FSMC_WAIT_SIGNAL_POLARITY_LOW;
  hsram1.Init.WrapMode = FSMC_WRAP_MODE_DISABLE;
  hsram1.Init.WaitSignalActive = FSMC_WAIT_TIMING_BEFORE_WS;
  hsram1.Init.WriteOperation = FSMC_WRITE_OPERATION_ENABLE;
  hsram1.Init.WaitSignal = FSMC_WAIT_SIGNAL_DISABLE;
  hsram1.Init.ExtendedMode = FSMC_EXTENDED_MODE_ENABLE;
  hsram1.Init.AsynchronousWait = FSMC_ASYNCHRONOUS_WAIT_DISABLE;
  hsram1.Init.WriteBurst = FSMC_WRITE_BURST_DISABLE;
  hsram1.Init.PageSize = FSMC_PAGE_SIZE_NONE;
  /* Timing */
  Timing.AddressSetupTime = 15;
  Timing.AddressHoldTime = 15;
  Timing.DataSetupTime = 60;
  Timing.BusTurnAroundDuration = 0;
  Timing.CLKDivision = 16;
  Timing.DataLatency = 17;
  Timing.AccessMode = FSMC_ACCESS_MODE_A;
  /* ExtTiming */
  ExtTiming.AddressSetupTime = 9;
  ExtTiming.AddressHoldTime = 15;
  ExtTiming.DataSetupTime = 8;
  ExtTiming.BusTurnAroundDuration = 0;
  ExtTiming.CLKDivision = 16;
  ExtTiming.DataLatency = 17;
  ExtTiming.AccessMode = FSMC_ACCESS_MODE_A;

  if (HAL_SRAM_Init(&hsram1, &Timing, &ExtTiming) != HAL_OK)
  {
    Error_Handler( );
  }

  /* USER CODE BEGIN FSMC_Init 2 */

  /* USER CODE END FSMC_Init 2 */
}


uint32_t FSMC_Initialized = 0;

static void HAL_FSMC_MspInit(void){
  /* USER CODE BEGIN FSMC_MspInit 0 */

  /* USER CODE END FSMC_MspInit 0 */
  GPIO_InitTypeDef GPIO_InitStruct ={0};
  if (FSMC_Initialized) {
    return;
  }
  FSMC_Initialized = 1;
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  /* Peripheral clock enable */
  __HAL_RCC_FSMC_CLK_ENABLE();

  /** FSMC GPIO Configuration
  PE7   ------> FSMC_D4
  PE8   ------> FSMC_D5
  PE9   ------> FSMC_D6
  PE10   ------> FSMC_D7
  PE11   ------> FSMC_D8
  PE12   ------> FSMC_D9
  PE13   ------> FSMC_D10
  PE14   ------> FSMC_D11
  PE15   ------> FSMC_D12
  PD8   ------> FSMC_D13
  PD9   ------> FSMC_D14
  PD10   ------> FSMC_D15
  PD11   ------> FSMC_A16
  PD14   ------> FSMC_D0
  PD15   ------> FSMC_D1
  PD0   ------> FSMC_D2
  PD1   ------> FSMC_D3
  PD4   ------> FSMC_NOE
  PD5   ------> FSMC_NWE
  PG12   ------> FSMC_NE4
  */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10
                          |GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14
                          |GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
                          |GPIO_PIN_14|GPIO_PIN_15|GPIO_PIN_0|GPIO_PIN_1
                          |GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  GPIO_InitStruct.Pin = GPIO_PIN_12;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /* USER CODE BEGIN FSMC_MspInit 1 */
   //初始化PF12
	GPIO_InitStruct.Pin=GPIO_PIN_12;
	HAL_GPIO_Init(GPIOF,&GPIO_InitStruct);
  /* USER CODE END FSMC_MspInit 1 */
}

void HAL_SRAM_MspInit(SRAM_HandleTypeDef* hsram){
  /* USER CODE BEGIN SRAM_MspInit 0 */

  /* USER CODE END SRAM_MspInit 0 */
  HAL_FSMC_MspInit();
  /* USER CODE BEGIN SRAM_MspInit 1 */

  /* USER CODE END SRAM_MspInit 1 */
}

static uint32_t FSMC_DeInitialized = 0;

static void HAL_FSMC_MspDeInit(void){
  /* USER CODE BEGIN FSMC_MspDeInit 0 */

  /* USER CODE END FSMC_MspDeInit 0 */
  if (FSMC_DeInitialized) {
    return;
  }
  FSMC_DeInitialized = 1;
  /* Peripheral clock enable */
  __HAL_RCC_FSMC_CLK_DISABLE();

  /** FSMC GPIO Configuration
  PE7   ------> FSMC_D4
  PE8   ------> FSMC_D5
  PE9   ------> FSMC_D6
  PE10   ------> FSMC_D7
  PE11   ------> FSMC_D8
  PE12   ------> FSMC_D9
  PE13   ------> FSMC_D10
  PE14   ------> FSMC_D11
  PE15   ------> FSMC_D12
  PD8   ------> FSMC_D13
  PD9   ------> FSMC_D14
  PD10   ------> FSMC_D15
  PD11   ------> FSMC_A16
  PD14   ------> FSMC_D0
  PD15   ------> FSMC_D1
  PD0   ------> FSMC_D2
  PD1   ------> FSMC_D3
  PD4   ------> FSMC_NOE
  PD5   ------> FSMC_NWE
  PG12   ------> FSMC_NE4
  */
  HAL_GPIO_DeInit(GPIOE, GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10
                          |GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14
                          |GPIO_PIN_15);

  HAL_GPIO_DeInit(GPIOD, GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
                          |GPIO_PIN_14|GPIO_PIN_15|GPIO_PIN_0|GPIO_PIN_1
                          |GPIO_PIN_4|GPIO_PIN_5);

  HAL_GPIO_DeInit(GPIOG, GPIO_PIN_12);

  /* USER CODE BEGIN FSMC_MspDeInit 1 */

  /* USER CODE END FSMC_MspDeInit 1 */
}

void HAL_SRAM_MspDeInit(SRAM_HandleTypeDef* hsram){
  /* USER CODE BEGIN SRAM_MspDeInit 0 */

  /* USER CODE END SRAM_MspDeInit 0 */
  HAL_FSMC_MspDeInit();
  /* USER CODE BEGIN SRAM_MspDeInit 1 */

  /* USER CODE END SRAM_MspDeInit 1 */
}

void LCD_Write_REG(uint16_t val)
{
	val=val;		//使用-O2优化的时候,必须插入的延时
	LCD->LCD_CMD=val;//写入要写的寄存器序号	
}
void LCD_Write_Data(uint16_t val)
{
	val=val;		//使用-O2优化的时候,必须插入的延时
	LCD->LCD_DATA=val;//写入要写的寄存器序号	
}
uint16_t LCD_Read_Data(void)
{
	uint16_t val;		//使用-O2优化的时候,必须插入的延时
	val=LCD->LCD_DATA;//写入要写的寄存器序号	
	return val;
}
void LCD_WriteReg(uint16_t LCD_Reg,uint16_t LCD_RegValue)
{	
	LCD->LCD_CMD = LCD_Reg;		//写入要写的寄存器序号	 
	LCD->LCD_DATA = LCD_RegValue;//写入数据	    		 
}	   
//读寄存器
//LCD_Reg:寄存器地址
//返回值:读到的数据
uint16_t LCD_ReadReg(uint16_t LCD_Reg)
{										   
	LCD_Write_REG(LCD_Reg);		//写入要读的寄存器序号
	delay_us(5);		  
	return LCD_Read_Data();		//返回读到的值
}
void LCD_SetCursor(uint16_t x,uint16_t y)
{
	LCD_Write_REG(0x2A);
	LCD_Write_Data(x>>8);
	LCD_Write_Data(x&0xff);
	LCD_Write_REG(0x2B);
	LCD_Write_Data(y>>8);
	LCD_Write_Data(y&0xff);
}
void opt_delay(uint8_t i)
{
	while(i--);
}
uint16_t Read_Point_Color(uint16_t x,uint16_t y)
{
	uint16_t rgb,m,n=0;
	
	LCD_SetCursor(x,y);
	LCD_Write_REG(0x2E);
	m=LCD_Read_Data();
	opt_delay(2);
	m=LCD_Read_Data();
	opt_delay(2);
	n=LCD_Read_Data();

	rgb=m|(m<<3)|(n>>11);
	return rgb;
}
void LCD_Display_On(void)
{
	LCD_Write_REG(0X29);
}
void LCD_Display_Off(void)
{
	LCD_Write_REG(0X28);
}
void LCD_Scan_Dir()
{
	uint16_t regval=0;
	uint16_t temp;
	uint16_t dir=0;
	if(lcd_dev.dir==1)
	{
		switch(DFT_SCAN_DIR)//方向转换
		{
			case 0:dir=6;break;
			case 1:dir=7;break;
			case 2:dir=4;break;
			case 3:dir=5;break;
			case 4:dir=1;break;
			case 5:dir=0;break;
			case 6:dir=3;break;
			case 7:dir=2;break;	     
		}	
	}
	switch(dir)
	{
		case L2R_U2D://从左到右,从上到下
			regval|=(0<<7)|(0<<6)|(0<<5); 
			break;
		case L2R_D2U://从左到右,从下到上
			regval|=(1<<7)|(0<<6)|(0<<5); 
			break;
		case R2L_U2D://从右到左,从上到下
			regval|=(0<<7)|(1<<6)|(0<<5); 
			break;
		case R2L_D2U://从右到左,从下到上
			regval|=(1<<7)|(1<<6)|(0<<5); 
			break;	 
		case U2D_L2R://从上到下,从左到右
			regval|=(0<<7)|(0<<6)|(1<<5); 
			break;
		case U2D_R2L://从上到下,从右到左
			regval|=(0<<7)|(1<<6)|(1<<5); 
			break;
		case D2U_L2R://从下到上,从左到右
			regval|=(1<<7)|(0<<6)|(1<<5); 
			break;
		case D2U_R2L://从下到上,从右到左
			regval|=(1<<7)|(1<<6)|(1<<5); 
			break;	 
	}
	LCD_WriteReg(0x36,regval|0x08);
	if(regval&0X20)
	{
		if(lcd_dev.width<lcd_dev.height)//交换X,Y
		{
			temp=lcd_dev.width;
			lcd_dev.width=lcd_dev.height;
			lcd_dev.height=temp;
		}
	}else  
	{
		if(lcd_dev.width>lcd_dev.height)//交换X,Y
		{
			temp=lcd_dev.width;
			lcd_dev.width=lcd_dev.height;
			lcd_dev.height=temp;
		}
	} 
	LCD_Write_REG(0x2A); 
	LCD_Write_Data(0);LCD_Write_Data(0);
	LCD_Write_Data((lcd_dev.width-1)>>8);LCD_Write_Data((lcd_dev.width-1)&0XFF);
	LCD_Write_REG(0x2B); 
	LCD_Write_Data(0);LCD_Write_Data(0);
	LCD_Write_Data((lcd_dev.height-1)>>8);LCD_Write_Data((lcd_dev.height-1)&0XFF);  

}

void LCD_DrawPoint(uint16_t x,uint16_t y,uint16_t color)
{ 
	LCD_SetCursor(x,y);		//设置光标位置 
	LCD_Write_REG(0x2C);	//开始写入GRAM
	LCD->LCD_DATA=color;  
}
void LCD_SSD_BackLightSet(uint8_t pwm)
{	
	LCD_Write_REG(0xBE);	//配置PWM输出
	LCD_Write_Data(0x05);	//1设置PWM频率
	LCD_Write_Data(pwm*2.55);//2设置PWM占空比
	LCD_Write_Data(0x01);	//3设置C
	LCD_Write_Data(0xFF);	//4设置D
	LCD_Write_Data(0x00);	//5设置E
	LCD_Write_Data(0x00);	//6设置F
}
//设置LCD显示方向
//dir:0,竖屏；1,横屏
void LCD_Display_Dir(uint8_t dir)
{
	lcd_dev.dir=dir;		//竖屏/横屏 
	if(dir==0)			//竖屏
	{
		lcd_dev.width=240;
		lcd_dev.height=320;
	}else 				//横屏
	{	  				 
		lcd_dev.width=320;
		lcd_dev.height=240;
	} 
	LCD_Scan_Dir();	//默认扫描方向
}
//设置窗口(对RGB屏无效),并自动设置画点坐标到窗口左上角(sx,sy).
//sx,sy:窗口起始坐标(左上角)
//width,height:窗口宽度和高度,必须大于0!!
//窗体大小:width*height. 
void LCD_Set_Window(uint16_t sx,uint16_t sy,uint16_t width,uint16_t height)
{     
	uint16_t twidth,theight;
	twidth=sx+width-1;
	theight=sy+height-1;
	LCD_Write_REG(0x2A); 
	LCD_Write_Data(sx>>8); 
	LCD_Write_Data(sx&0XFF);	 
	LCD_Write_Data(twidth>>8); 
	LCD_Write_Data(twidth&0XFF);  
	LCD_Write_REG(0x2B); 
	LCD_Write_Data(sy>>8); 
	LCD_Write_Data(sy&0XFF); 
	LCD_Write_Data(theight>>8); 
	LCD_Write_Data(theight&0XFF); 
}
void LCD_Clear(uint16_t color)
{
	uint32_t  index=0;      
	uint32_t totalpoint=lcd_dev.width; 
	totalpoint*=lcd_dev.height; 			//得到总点数
	LCD_SetCursor(0x00,0x0000);			//设置光标位置 
	LCD_Write_REG(0x2C);  		//开始写入GRAM	 	  
	for(index=0;index<totalpoint;index++)
	{
		LCD->LCD_DATA=color;	
	} 
}
//在指定区域内填充单个颜色
//(sx,sy),(ex,ey):填充矩形对角坐标,区域大小为:(ex-sx+1)*(ey-sy+1)   
//color:要填充的颜色
void LCD_Fill(uint16_t x1,uint16_t y1,uint16_t x2,uint16_t y2,uint16_t color)
{          
	uint16_t i,j;
	uint16_t xlen=0; 
	xlen=x2-x1+1;	 
	for(i=y1;i<=y2;i++)
	{
		LCD_SetCursor(x1,i);      				//设置光标位置 
		LCD_Write_REG(0x2C);     			//开始写入GRAM	  
		for(j=0;j<xlen;j++)LCD->LCD_DATA=color;	//显示颜色 	    
	}  
} 
void LCD_Color_Fill(uint16_t sx,uint16_t sy,uint16_t ex,uint16_t ey,uint16_t *color)
{  
	uint16_t height,width;
	uint16_t i,j;
	width=ex-sx+1; 			//得到填充的宽度
	height=ey-sy+1;			//高度
 	for(i=0;i<height;i++)
	{
 		LCD_SetCursor(sx,sy+i);   	//设置光标位置 
		LCD_Write_REG(0x2C);     //开始写入GRAM
		for(j=0;j<width;j++)LCD->LCD_DATA=color[i*width+j];//写入数据 
	}		  
}
//画线
//x1,y1:起点坐标
//x2,y2:终点坐标  
void LCD_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,uint16_t color)
{
	uint16_t t; 
	int xerr=0,yerr=0,delta_x,delta_y,distance; 
	int incx,incy,uRow,uCol; 
	delta_x=x2-x1; //计算坐标增量 
	delta_y=y2-y1; 
	uRow=x1; 
	uCol=y1; 
	if(delta_x>0)incx=1; //设置单步方向 
	else if(delta_x==0)incx=0;//垂直线 
	else {incx=-1;delta_x=-delta_x;} 
	if(delta_y>0)incy=1; 
	else if(delta_y==0)incy=0;//水平线 
	else{incy=-1;delta_y=-delta_y;} 
	if( delta_x>delta_y)distance=delta_x; //选取基本增量坐标轴 
	else distance=delta_y; 
	for(t=0;t<=distance+1;t++ )//画线输出 
	{  
		LCD_DrawPoint(uRow,uCol,color);//画点 
		xerr+=delta_x ; 
		yerr+=delta_y ; 
		if(xerr>distance) 
		{ 
			xerr-=distance; 
			uRow+=incx; 
		} 
		if(yerr>distance) 
		{ 
			yerr-=distance; 
			uCol+=incy; 
		} 
	}  
} 
//画矩形	  
//(x1,y1),(x2,y2):矩形的对角坐标
void LCD_DrawRectangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2,uint16_t color)
{
	LCD_DrawLine(x1,y1,x2,y1,color);
	LCD_DrawLine(x1,y1,x1,y2,color);
	LCD_DrawLine(x1,y2,x2,y2,color);
	LCD_DrawLine(x2,y1,x2,y2,color);
}
//在指定位置画一个指定大小的圆
//(x,y):中心点
//r    :半径
void LCD_Draw_Circle(uint16_t x0,uint16_t y0,uint8_t r,uint16_t color)
{
	int a,b;
	int di;
	a=0;b=r;	  
	di=3-(r<<1);             //判断下个点位置的标志
	while(a<=b)
	{
		LCD_DrawPoint(x0+a,y0-b,color);             //5
 		LCD_DrawPoint(x0+b,y0-a,color);             //0           
		LCD_DrawPoint(x0+b,y0+a,color);             //4               
		LCD_DrawPoint(x0+a,y0+b,color);             //6 
		LCD_DrawPoint(x0-a,y0+b,color);             //1       
 		LCD_DrawPoint(x0-b,y0+a,color);             
		LCD_DrawPoint(x0-a,y0-b,color);             //2             
  		LCD_DrawPoint(x0-b,y0-a,color);             //7     	         
		a++;
		//使用Bresenham算法画圆     
		if(di<0)di +=4*a+6;	  
		else
		{
			di+=10+4*(a-b);   
			b--;
		} 						    
	}
} 									  
//在指定位置显示一个字符
//x,y:起始坐标
//num:要显示的字符:" "--->"~"
//size:字体大小 12/16/24/32
//mode:叠加方式(1)还是非叠加方式(0)
uint8_t ascii_buf[80]={0};
void LCD_ShowChar(uint16_t x,uint16_t y,uint8_t num,uint8_t size,uint16_t color)
{  		
    uint8_t temp,t1,t;
	uint16_t y0=y;
	uint8_t csize=(size/8+((size%8)?1:0))*(size/2);		//得到字体一个字符对应点阵集所占的字节数	
 	num=num-' ';//得到偏移后的值（ASCII字库是从空格开始取模，所以-' '就是对应字符的字库）
	uint32_t ascii_addr=0;
	rt_device_t dev=rt_device_find("spi_flash");
	if(dev==NULL)
		return;
	if(update_flashfont()==false)
		return;
	switch (size)
    {
    	case 12:
			ascii_addr=show_zi_addr.ascii_12_addr;
    		break;
    	case 16:
			ascii_addr=show_zi_addr.ascii_16_addr;
    		break;
    	case 24:
			ascii_addr=show_zi_addr.ascii_24_addr;
    		break;
    	case 32:
			ascii_addr=show_zi_addr.ascii_32_addr;
    		break;
    }
	rt_device_read(dev,ascii_addr+num*csize,ascii_buf,csize);
	for(t=0;t<csize;t++)
	{   
//		if(size==12)temp=asc2_1206[num][t]; 	 	//调用1206字体
//		else if(size==16)temp=asc2_1608[num][t];	//调用1608字体
//		else if(size==24)temp=asc2_2412[num][t];	//调用2412字体
//		else if(size==32)temp=asc2_3216[num][t];	//调用3216字体
//		else return;
		if(size==12)temp=ascii_buf[t]; 	 	//调用1206字体
		else if(size==16)temp=ascii_buf[t];	//调用1608字体
		else if(size==24)temp=ascii_buf[t];	//调用2412字体
		else if(size==32)temp=ascii_buf[t];	//调用3216字体
		else return;		//没有的字库
		for(t1=0;t1<8;t1++)
		{			    
			if(temp&0x80)LCD_DrawPoint(x,y,color);
			temp<<=1;
			y++;
			if(y>=lcd_dev.height)return;		//超区域了
			if((y-y0)==size)
			{
				y=y0;
				x++;
				if(x>=lcd_dev.width)return;	//超区域了
				break;
			}
		}  	 
	}  	    	   	 	  
}   
//m^n函数
//返回值:m^n次方.
uint32_t LCD_Pow(uint8_t m,uint8_t n)
{
	uint32_t result=1;	 
	while(n--)result*=m;    
	return result;
}			 
//显示数字,高位为0,则不显示
//x,y :起点坐标	 
//len :数字的位数
//size:字体大小
//color:颜色 
//num:数值(0~4294967295);	 
void LCD_ShowNum(uint16_t x,uint16_t y,uint32_t num,uint8_t len,uint8_t size,uint16_t color)
{         	
	uint8_t t,temp;
	uint8_t enshow=0;						   
	for(t=0;t<len;t++)
	{
		temp=(num/LCD_Pow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				LCD_ShowChar(x+(size/2)*t,y,' ',size,color);
				continue;
			}else enshow=1; 
		 	 
		}
	 	LCD_ShowChar(x+(size/2)*t,y,temp+'0',size,color); 
	}
} 
//显示数字,高位为0,还是显示
//x,y:起点坐标
//num:数值(0~999999999);	 
//len:长度(即要显示的位数)
//size:字体大小

void LCD_ShowxNum(uint16_t x,uint16_t y,uint32_t num,uint8_t len,uint8_t size,uint16_t color)
{  
	uint8_t t,temp;
	uint8_t enshow=0;						   
	for(t=0;t<len;t++)
	{
		temp=(num/LCD_Pow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				LCD_ShowChar(x+(size/2)*t,y,'0',size,color);   
 				continue;
			}else enshow=1; 
		 	 
		}
	 	LCD_ShowChar(x+(size/2)*t,y,temp+'0',size,color); 
	}
} 
//显示字符串
//x,y:起点坐标
//width,height:区域大小  
//size:字体大小
//*p:字符串起始地址		  
void LCD_ShowString(uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t size,uint8_t *p,uint16_t color)
{         
	uint8_t x0=x;
	width+=x;
	height+=y;
    while((*p<='~')&&(*p>=' '))//判断是不是非法字符!
    {       
        if(x>=width){x=x0;y+=size;}
        if(y>=height)break;//退出
        LCD_ShowChar(x,y,*p,size,color);
        x+=size/2;
        p++;
    }  
}

extern uint8_t TP_Init(void);
int LCD_Init()
{
	GPIO_InitTypeDef GPIO_Initure;
	__HAL_RCC_GPIOB_CLK_ENABLE();			//开启GPIOB时钟
	GPIO_Initure.Pin=GPIO_PIN_15;          	//PB15,背光控制
	GPIO_Initure.Mode=GPIO_MODE_OUTPUT_PP;  //推挽输出
	GPIO_Initure.Pull=GPIO_PULLUP;          //上拉
	GPIO_Initure.Speed=GPIO_SPEED_HIGH;     //高速
	HAL_GPIO_Init(GPIOB,&GPIO_Initure); 
	MX_FSMC_Init();
	delay_ms(50);
	
	LCD_Write_REG(0XD3);				   
	lcd_dev.id=LCD_Read_Data();	//dummy read 	
	lcd_dev.id=LCD_Read_Data();	//读到0X00
	lcd_dev.id=LCD_Read_Data();   	//读取93								   
	lcd_dev.id<<=8;
	lcd_dev.id|=LCD_Read_Data();  	//读取41
	if(lcd_dev.id==0X9341)	//9341初始化
	{	 
		LCD_Write_REG(0xCF);  
		LCD_Write_Data(0x00); 
		LCD_Write_Data(0xC1); 
		LCD_Write_Data(0X30); 
		LCD_Write_REG(0xED);  
		LCD_Write_Data(0x64); 
		LCD_Write_Data(0x03); 
		LCD_Write_Data(0X12); 
		LCD_Write_Data(0X81); 
		LCD_Write_REG(0xE8);  
		LCD_Write_Data(0x85); 
		LCD_Write_Data(0x10); 
		LCD_Write_Data(0x7A); 
		LCD_Write_REG(0xCB);  
		LCD_Write_Data(0x39); 
		LCD_Write_Data(0x2C); 
		LCD_Write_Data(0x00); 
		LCD_Write_Data(0x34); 
		LCD_Write_Data(0x02); 
		LCD_Write_REG(0xF7);  
		LCD_Write_Data(0x20); 
		LCD_Write_REG(0xEA);  
		LCD_Write_Data(0x00); 
		LCD_Write_Data(0x00); 
		LCD_Write_REG(0xC0);    //Power control 
		LCD_Write_Data(0x1B);   //VRH[5:0] 
		LCD_Write_REG(0xC1);    //Power control 
		LCD_Write_Data(0x01);   //SAP[2:0];BT[3:0] 
		LCD_Write_REG(0xC5);    //VCM control 
		LCD_Write_Data(0x30); 	 //3F
		LCD_Write_Data(0x30); 	 //3C
		LCD_Write_REG(0xC7);    //VCM control2 
		LCD_Write_Data(0XB7); 
		LCD_Write_REG(0x36);    // Memory Access Control 
		LCD_Write_Data(0x48); 
		LCD_Write_REG(0x3A);   
		LCD_Write_Data(0x55); 
		LCD_Write_REG(0xB1);   
		LCD_Write_Data(0x00);   
		LCD_Write_Data(0x1A); 
		LCD_Write_REG(0xB6);    // Display Function Control 
		LCD_Write_Data(0x0A); 
		LCD_Write_Data(0xA2); 
		LCD_Write_REG(0xF2);    // 3Gamma Function Disable 
		LCD_Write_Data(0x00); 
		LCD_Write_REG(0x26);    //Gamma curve selected 
		LCD_Write_Data(0x01); 
		LCD_Write_REG(0xE0);    //Set Gamma 
		LCD_Write_Data(0x0F); 
		LCD_Write_Data(0x2A); 
		LCD_Write_Data(0x28); 
		LCD_Write_Data(0x08); 
		LCD_Write_Data(0x0E); 
		LCD_Write_Data(0x08); 
		LCD_Write_Data(0x54); 
		LCD_Write_Data(0XA9); 
		LCD_Write_Data(0x43); 
		LCD_Write_Data(0x0A); 
		LCD_Write_Data(0x0F); 
		LCD_Write_Data(0x00); 
		LCD_Write_Data(0x00); 
		LCD_Write_Data(0x00); 
		LCD_Write_Data(0x00); 		 
		LCD_Write_REG(0XE1);    //Set Gamma 
		LCD_Write_Data(0x00); 
		LCD_Write_Data(0x15); 
		LCD_Write_Data(0x17); 
		LCD_Write_Data(0x07); 
		LCD_Write_Data(0x11); 
		LCD_Write_Data(0x06); 
		LCD_Write_Data(0x2B); 
		LCD_Write_Data(0x56); 
		LCD_Write_Data(0x3C); 
		LCD_Write_Data(0x05); 
		LCD_Write_Data(0x10); 
		LCD_Write_Data(0x0F); 
		LCD_Write_Data(0x3F); 
		LCD_Write_Data(0x3F); 
		LCD_Write_Data(0x0F); 
		LCD_Write_REG(0x2B); 
		LCD_Write_Data(0x00);
		LCD_Write_Data(0x00);
		LCD_Write_Data(0x01);
		LCD_Write_Data(0x3f);
		LCD_Write_REG(0x2A); 
		LCD_Write_Data(0x00);
		LCD_Write_Data(0x00);
		LCD_Write_Data(0x00);
		LCD_Write_Data(0xef);	 
		LCD_Write_REG(0x11); //Exit Sleep
		delay_ms(120);
		LCD_Write_REG(0x29); //display on	
	}
	 //初始化完成以后,提速
	if(lcd_dev.id==0X9341)//如果是这几个IC,则设置WR时序为最快
	{
		//重新配置写时序控制寄存器的时序   	 							    
		FSMC_Bank1E->BWTR[6]&=~(0XF<<0);//地址建立时间(ADDSET)清零 	 
		FSMC_Bank1E->BWTR[6]&=~(0XF<<8);//数据保存时间清零
		FSMC_Bank1E->BWTR[6]|=3<<0;		//地址建立时间(ADDSET)为3个HCLK =18ns  	 
		FSMC_Bank1E->BWTR[6]|=2<<8; 	//数据保存时间(DATAST)为6ns*3个HCLK=18ns
	}
	LCD_Display_Dir(0);		//默认为竖屏
	LCD_LED=1;				//点亮背光
	TP_Init();	
	return 1;
}
int LCD_DeInit()
{
	LCD_LED=0;
	HAL_FSMC_MspDeInit();
	HAL_SRAM_MspDeInit(&hsram1);
	return 1;
}
















