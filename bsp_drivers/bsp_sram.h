#ifndef BSP_SRAM_H
#define BSP_SRAM_H

#include "stm32f4xx_hal.h"
#define Bank1_SRAM3_ADDR 0x68000000
#define Bank1_SRAM3_SIZE (1024*1024)

extern void FSMC_SRAM_WriteBuffer(uint8_t *pBuffer,uint32_t WriteAddr,uint32_t n);
extern void FSMC_SRAM_ReadBuffer(uint8_t *pBuffer,uint32_t ReadAddr,uint32_t n);


extern void MX_SRAM_DeInit(void);
extern int MX_SRAM_Init(void);
extern void SRAM_Test(void);
#endif














