#include "bsp_led.h"

#define device  struct rt_device
device led;

rt_err_t led_init(device *dev)
{
	GPIO_InitTypeDef gpio_struct;
	__HAL_RCC_GPIOF_CLK_ENABLE();
	gpio_struct.Mode=GPIO_MODE_OUTPUT_PP;
	gpio_struct.Pin=GPIO_PIN_9;
	gpio_struct.Pull=GPIO_NOPULL;
	gpio_struct.Speed=GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOF,&gpio_struct);
	gpio_struct.Pin=GPIO_PIN_10;
	HAL_GPIO_Init(GPIOF,&gpio_struct);
	PIN_OUT(F,9)=1;
	PIN_OUT(F,10)=1;
	return RT_EOK;
}

rt_err_t led_contorl(device *dev,int cmd,void *args)
{
	
	switch(cmd)
	{
		case LED0_ON:
				PIN_OUT(F,9)=0;
				break;
		case LED0_OFF:
				PIN_OUT(F,9)=1;
				break;
		case LED0_TOGGLE:
				PIN_OUT(F,9)=~PIN_IN(F,9);
				break;
		case LED1_ON:
				PIN_OUT(F,10)=0;
				break;
		case LED1_OFF:
				PIN_OUT(F,10)=1;
				break;
		case LED1_TOGGLE:
				PIN_OUT(F,10)=~PIN_IN(F,10);
				break;
	}
	
	return RT_EOK;
}
int led_device_register()
{
	led.init=led_init;
	led.control=led_contorl;
	return rt_device_register(&led,"LED",RT_DEVICE_FLAG_RDWR);  
}

DEVICE_INIT(led_device_register);




