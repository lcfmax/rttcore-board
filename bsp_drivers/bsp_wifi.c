#include "bsp_wifi.h"
#include "tool.h"
#include "Dev_usart.h"

#define COM_AP_STA
//#define COM_AP

ESP8266_TABLE ESP8266_table[]= {
#ifdef COM_AP
    {"AT\r\n",					"OK",	"ERROR",500,3},
    {"AT+CWMODE=2\r\n",			"OK",	"ERROR",500,3},
    {"AT+RST\r\n",				"OK",	"ERROR",500,3},
    {"AT+CWSAP=\"ATK-ESP8266\",\"12345678\",1,4\r\n",	"OK",	"ERROR",500,3},
    {"AT+CIPMUX=1\r\n",				"OK",	"ERROR",500,3},
    {"AT+CIPSERVER=1,8086\r\n","OK",	"ERROR",500,3},
    {"AT+CIFSR\r\n",				"OK",	"ERROR",500,3},
    {"完成\r\n",					"OK",	"ERROR",500,3},
#endif
#ifdef 	COM_AP_STA
    {"AT\r\n",					"OK",	"ERROR",1000,3},
    {"AT+CWMODE=3\r\n",			"OK",	"ERROR",10000,3},
    {"AT+RST\r\n",				"OK",	"ERROR",10000,3},
    {"AT+CWSAP=\"ATK-ESP8266\",\"12345678\",1,4\r\n",	"OK",	"ERROR",500,3},
    {"AT+CWJAP=\"ALIENTEK\",\"15902020353\"\r\n",	"OK",	"FAIL",20000,3},
    {"AT+CIPMUX=1\r\n",		    "OK",	"ERROR",1000,3},
    {"AT+CIPSERVER=1,8086\r\n", "OK",	"ERROR",1000,3},
    {"AT+CIPSTO=1200\r\n",		"OK",	"ERROR",1000,3},
    {"AT+CIFSR\r\n",			"OK",	"ERROR",1000,3},
    {"完成\r\n",                "OK",	 "ERROR",1000,3},
#endif
};
#define ESP8266_SIZE (sizeof(ESP8266_table)/sizeof(ESP8266_TABLE))

DEV_ESP8266 Dev_Esp8266;
static void wait_cmd_ack(void);
static void Wifi_recvData_handle(void);
CONNECT_DEVICE_INFO* Wifi_Get_ConnectDeviceInfo(void);
void ESP8266_timeout(void *parameter)
{
	*(uint8_t*)parameter=1;
    // 
}
static void wifi_send_entry(void *parameter)
{
    while(1)
    {
		if(rt_sem_take(Dev_Esp8266.tx_sem,RT_WAITING_FOREVER)==RT_EOK)
		{
			Uart_Write(Dev_Esp8266.uart_dev,Dev_Esp8266.tx_data_point,Dev_Esp8266.tx_data_len);
		}
		rt_thread_delay(3);
    }
}
static void wifi_recv_entry(void *parameter)
{
 
    while (1)
    {
       switch (Dev_Esp8266.step)
       {
        case 0:
            if(Uart_Write(Dev_Esp8266.uart_dev,(uint8_t*)Dev_Esp8266.esp8266_table[Dev_Esp8266.AT_index].cmd,
                            strlen(Dev_Esp8266.esp8266_table[Dev_Esp8266.AT_index].cmd))==RT_EOK)
                Dev_Esp8266.step=1;
            else
                Dev_Esp8266.step=3;
             break;
        case 1:
             wait_cmd_ack();
             break;
        case 2:
			 Wifi_recvData_handle();
             break;
        case 3:
             rt_thread_suspend(Dev_Esp8266.Esp_rx_tid);
             break;
       default:
           break;
       }
        rt_thread_delay(3);
    }
}
static void Get_locateIp(uint8_t *InBuf,int InLen)
{
	uint8_t type;
    int len=0;
	Get_value_by_key((char*)InBuf,InLen,"CIFSR:APIP",Dev_Esp8266.located_ip.APIP,NULL,&type,&len);
	if(type==1&&len!=0)
	{
		rt_kprintf("APIP是 %s\r\n",Dev_Esp8266.located_ip.APIP);
	}          
	Get_value_by_key((char*)InBuf,InLen,"CIFSR:STAIP",Dev_Esp8266.located_ip.STAIP,NULL,&type,&len);
	if(type==1&&len!=0)
	{
		rt_kprintf("STAIP是 %s\r\n",Dev_Esp8266.located_ip.STAIP);
	}            
}
static void wait_cmd_ack(void)
{
    uint8_t flag=0;
    uint16_t rx_len=0;   
    flag=Uart_Get_Rx_State(Dev_Esp8266.uart_dev,&rx_len);
    if(flag==0&&rx_len!=0)
        rt_timer_start(Dev_Esp8266.Esp_timeOut_timer);  
    else if(flag==1&&rx_len!=0)
    {
        if(Dev_Esp8266.timeout_flag!=1)
			return;
        Dev_Esp8266.read_buf=my_alloc(rx_len+1);
        Uart_Read(Dev_Esp8266.uart_dev,Dev_Esp8266.read_buf,rx_len);
        if(KMP((char*)Dev_Esp8266.read_buf,rx_len,Dev_Esp8266.esp8266_table[Dev_Esp8266.AT_index].true_ack,
                    strlen(Dev_Esp8266.esp8266_table[Dev_Esp8266.AT_index].true_ack))!=-1)
                    {
						Get_locateIp(Dev_Esp8266.read_buf,rx_len);
                        my_free(Dev_Esp8266.read_buf);
                        if(Dev_Esp8266.AT_index==ESP8266_SIZE-1)
                        {
                            for(int i=0;i<ESP8266_SIZE;i++)
                                Dev_Esp8266.esp8266_table[i].retry_times=3;
                            Dev_Esp8266.step=2;
                            Dev_Esp8266.AT_index=0;
							Dev_Esp8266.timeout_flag=0;
                            return;
                        }
                        Dev_Esp8266.step=0;                            
                        Dev_Esp8266.AT_index++;
                    }
        else
        {
            my_free(Dev_Esp8266.read_buf);
            if(Dev_Esp8266.esp8266_table[Dev_Esp8266.AT_index].retry_times==0)
            {
                Dev_Esp8266.step=3;
                for(int i=0;i<ESP8266_SIZE;i++)
                    Dev_Esp8266.esp8266_table[i].retry_times=3;
				Dev_Esp8266.timeout_flag=0;
                return;
            }
            Dev_Esp8266.esp8266_table[Dev_Esp8266.AT_index].retry_times--;
            Dev_Esp8266.step=0; 
			Dev_Esp8266.timeout_flag=0;
        }     
    } 
}
static void recv_data_handle(char *recv_data,int recv_len)
{
    int i=0;
	uint8_t index=0;
    uint8_t connect_device_offest=0;
	uint8_t find_index=0;
	uint8_t douhao[20]={0};
	if(KMP(recv_data,recv_len,"busy:",strlen("busy"))!=-1)
    {
        rt_timer_start(Dev_Esp8266.Esp_timeOut_timer);  
        return;
    }
    if(recv_data[8]==':')
    {
        connect_device_offest=0;
		rt_ringbuffer_put_force(Dev_Esp8266.esp8266_recv_data[connect_device_offest].rx_ringbuffer,(uint8_t*)&recv_data[9],recv_len-9);
        Dev_Esp8266.esp8266_recv_data[connect_device_offest].recv_len=recv_len-9;
		Dev_Esp8266.esp8266_recv_data[connect_device_offest].ready_read=1;
		rt_sem_release(Dev_Esp8266.read_sem);
		return;
    }
    else if(recv_data[10]==':')
    {
        connect_device_offest=atoi((char*)&recv_data[5]);
		rt_ringbuffer_put_force(Dev_Esp8266.esp8266_recv_data[connect_device_offest].rx_ringbuffer,(uint8_t*)&recv_data[11],recv_len-11);
        Dev_Esp8266.esp8266_recv_data[connect_device_offest].recv_len=recv_len-11;
		Dev_Esp8266.esp8266_recv_data[connect_device_offest].ready_read=1;
		rt_sem_release(Dev_Esp8266.read_sem);
		return;
    }

    if(strstr((char*)recv_data,"SEND OK")!=NULL)
    {
		Dev_Esp8266.tx_finish_flag=RT_EOK;
        if(Dev_Esp8266.dev.tx_complete!=RT_NULL)
			Dev_Esp8266.dev.tx_complete(&Dev_Esp8266.dev,&Dev_Esp8266.tx_finish_flag);
        return;
    }
	if(strstr((char*)recv_data,"STATUS")!=NULL&&strstr((char*)recv_data,"OK")!=NULL)
	{
//		[21:37:47.239]收←◆AT+CIPSTATUS
//STATUS:5
//+CIPSTATUS:0,"TCP","192.168.4.2",52767,8086,1

//OK
		i=KMP(recv_data,recv_len,"STATUS:",strlen("STATUS:"));
		if(i!=-1)
		{
			Dev_Esp8266.esp8266_work_mode=atoi(&recv_data[i]);
			for(index=0;index<recv_len;index++)
			{
				if(recv_data[index]==',')
				{
					douhao[find_index++]=index;
				}
			}
			for(i=0;i<find_index;i+=5)
			{
				if(douhao[i]!=0)
				{
					index=atoi(&recv_data[douhao[i]-1]);
					memcpy(Dev_Esp8266.Connect_Device_Info[index].type,&recv_data[douhao[i]+2],3);
					memcpy(Dev_Esp8266.Connect_Device_Info[index].ip,&recv_data[douhao[i+1]+2],douhao[i+2]-douhao[i+1]-3);
					memcpy(Dev_Esp8266.Connect_Device_Info[index].remote_port,&recv_data[douhao[i+2]+1],douhao[i+3]-douhao[i+2]-1);
					memcpy(Dev_Esp8266.Connect_Device_Info[index].local_port,&recv_data[douhao[i+3]],douhao[i+4]-douhao[i+3]-1);
					memcpy(&Dev_Esp8266.Connect_Device_Info[index].connect_type,&recv_data[douhao[i+4]+1],1);
				}
				else
					break;
			}
			rt_sem_release(Dev_Esp8266.read_sem);	
		}
			
	}
    if(strstr((char*)recv_data,"OK")!=NULL&&strstr((char*)recv_data,">")!=NULL)
    {
        rt_sem_release(Dev_Esp8266.tx_sem);
        return;
    }
    if(strstr((char*)recv_data,"ERROR")!=NULL)
    {
		Dev_Esp8266.tx_finish_flag=RT_ERROR;
		if(Dev_Esp8266.dev.tx_complete!=RT_NULL)
			Dev_Esp8266.dev.tx_complete(&Dev_Esp8266.dev,&Dev_Esp8266.tx_finish_flag);
        return;
    }
}
static void Wifi_recvData_handle(void)
{
	uint8_t flag=0;
    uint16_t rx_len=0;
	flag=Uart_Get_Rx_State(Dev_Esp8266.uart_dev,&rx_len);
    if(flag==0&&rx_len!=0)
        rt_timer_start(Dev_Esp8266.Esp_timeOut_timer);  
    else if(flag==1&&rx_len!=0)
    {
        rt_thread_mdelay(10);
        Dev_Esp8266.read_buf=my_alloc(rx_len+1);
		Uart_Read(Dev_Esp8266.uart_dev,Dev_Esp8266.read_buf,rx_len);
		rt_kprintf("接收到数据字节数%d,内容为%s\r\n",rx_len,Dev_Esp8266.read_buf);
		recv_data_handle((char*)Dev_Esp8266.read_buf,rx_len);
		my_free(Dev_Esp8266.read_buf);
	}
}
rt_err_t ESP8266_Init(rt_device_t dev)
{
	Dev_Uart_Init(3);
    Dev_Esp8266.uart_dev=rt_device_find("uart3");
	
    if(Dev_Esp8266.uart_dev==NULL)
    {
        rt_kprintf("uart3查找失败\r\n");
        return RT_ERROR;
    }
    Dev_Esp8266.esp8266_table=ESP8266_table;
    Dev_Esp8266.Esp_rx_tid=rt_thread_create("wifi_rx",wifi_recv_entry,RT_NULL,512,5,10);
    Dev_Esp8266.Esp_tx_tid=rt_thread_create("wifi_tx",wifi_send_entry,RT_NULL,512,5,10);
    if(Dev_Esp8266.Esp_rx_tid==RT_NULL||Dev_Esp8266.Esp_tx_tid==RT_NULL)
    {
        rt_kprintf("wifi线程创建失败\r\n");
        return RT_ERROR;
    }
    Dev_Esp8266.tx_sem=rt_sem_create("wifi_sem",0,RT_IPC_FLAG_FIFO);
	Dev_Esp8266.read_sem=rt_sem_create("readwfsem",0,RT_IPC_FLAG_FIFO);
    Dev_Esp8266.Esp_timeOut_timer=rt_timer_create("wifi_timer",ESP8266_timeout,
														&Dev_Esp8266.timeout_flag,Dev_Esp8266.esp8266_table[0].timeout,
														RT_TIMER_FLAG_ONE_SHOT | RT_TIMER_FLAG_SOFT_TIMER);
    if(Dev_Esp8266.Esp_timeOut_timer==RT_NULL)
    {
        rt_kprintf("wifi定时器创建失败\r\n");
        return RT_ERROR;
    }
	for(int i=0;i<CONNECT_DEVICE_MAX;i++)
	{
		Dev_Esp8266.esp8266_recv_data[i].rx_ringbuffer=rt_ringbuffer_create(WIFI_RINGBUFFER_MAX);
		if(Dev_Esp8266.esp8266_recv_data[i].rx_ringbuffer==RT_NULL)
		{
			rt_kprintf("wifi缓冲区创建失败\r\n");
			return RT_ERROR;
		}
	}		
    rt_thread_startup(Dev_Esp8266.Esp_rx_tid);   
    rt_thread_startup(Dev_Esp8266.Esp_tx_tid);                                          
    return RT_EOK;
}
int Wifi_Get_Rx_Len(int connect_device_offest)
{
	return Dev_Esp8266.esp8266_recv_data[connect_device_offest].recv_len;
}
rt_size_t Wifi_Read(rt_device_t wifi_dev,rt_off_t connect_device_offest,void *read_buf,rt_size_t read_len)
{
	if(rt_sem_take(Dev_Esp8266.read_sem,RT_WAITING_FOREVER)==RT_EOK)
	{
		if(Dev_Esp8266.esp8266_recv_data[connect_device_offest].recv_len<read_len)
		read_len=Dev_Esp8266.esp8266_recv_data[connect_device_offest].recv_len;
		Dev_Esp8266.esp8266_recv_data[connect_device_offest].recv_len=0;	
		return rt_ringbuffer_get(Dev_Esp8266.esp8266_recv_data[connect_device_offest].rx_ringbuffer,(uint8_t*)read_buf,read_len);
	}
	else 
		return 0;
}
rt_size_t Wifi_Write(rt_device_t wifi_dev,rt_off_t connect_device_offest,const void *write_buf,rt_size_t write_len)
{
	char send_data[30]= {0};
	sprintf(send_data,"AT+CIPSEND=%d,%d\r\n",(int)connect_device_offest,(int)write_len);
	Uart_Write(Dev_Esp8266.uart_dev,(uint8_t*)send_data,strlen(send_data));
	Dev_Esp8266.tx_data_point=(uint8_t*)write_buf;
	Dev_Esp8266.tx_data_len=write_len;
	return write_len;
}
rt_err_t wifi_control(rt_device_t dev,int cmd,void *args)
{
    switch (cmd&0X0F)
    {
		case GET_WIFI_RX_LEN:
			*(int*)args=Wifi_Get_Rx_Len((cmd>>4)&0X0F);
			break;
		case QUERY_CONNECT_IP_PORT:
			args=Wifi_Get_ConnectDeviceInfo();
			break;
		default:
			break;
    }
    return RT_EOK;
}
int esp8266_register()
{
    Dev_Esp8266.dev.init=ESP8266_Init;
    Dev_Esp8266.dev.read=Wifi_Read;
    Dev_Esp8266.dev.write=Wifi_Write;
    Dev_Esp8266.dev.control=wifi_control;
	rt_device_register(&Dev_Esp8266.dev,"esp8266",RT_DEVICE_OFLAG_RDWR);
	return RT_EOK;
}
APP_INIT(esp8266_register);
CONNECT_DEVICE_INFO* Wifi_Get_ConnectDeviceInfo()
{
	uint8_t send_buf[]="AT+CIPSTATUS\r\n";
	Uart_Write(Dev_Esp8266.uart_dev,send_buf,strlen((char*)send_buf));
	if(rt_sem_take(Dev_Esp8266.read_sem,RT_WAITING_FOREVER)==RT_EOK)
	{
		return Dev_Esp8266.Connect_Device_Info;
	}
	else 
		return 0;
}
uint8_t wifitx_buf[30]={0};
void wifi_send_data(uint8_t argc, char **argv)
{
    rt_device_t wifi_dev=rt_device_find("esp8266");
    if(wifi_dev==NULL) return;
	rt_memset(wifitx_buf,0,30);
	rt_memcpy(wifitx_buf,argv[2],strlen(argv[2]));
    Wifi_Write(wifi_dev,atoi(argv[1]),wifitx_buf,strlen(argv[2]));
}
MSH_CMD_EXPORT(wifi_send_data,WIFI SEND DATA);







