#include "config.h"
#include "fatfstest.h"
#include "bsp_lcd.h"
#include "bsp_spiflash.h"
#define device  struct rt_device
extern unsigned char asc2_1206[95][12];
extern unsigned char asc2_1608[95][16];
extern unsigned char asc2_2412[95][36];
extern unsigned char asc2_3216[95][128];
//字库存放在磁盘中的路径
#define GBK24_PATH "/SYSTEM/FONT/GBK24.FON"		//GBK24的存放位置
#define GBK16_PATH "/SYSTEM/FONT/GBK16.FON"		//GBK16的存放位置
#define GBK12_PATH "/SYSTEM/FONT/GBK12.FON"		//GBK12的存放位置

uint8_t update_flashfont(void);

void Read_Font(uint8_t *buf,uint8_t fontsize,uint8_t *HZ)
{
	uint8_t csize=(fontsize/8+((fontsize%8)?1:0))*(fontsize);
	uint8_t ch_H=*HZ;
	uint8_t ch_L=*(HZ+1);
	uint32_t addr=0;
	
	if(ch_L<0x7f)ch_L-=0x40;//注意!
	else ch_L-=0x41;
	
	ch_H-=0x81;
	uint32_t foffset=((unsigned long)190*ch_H+ch_L)*csize;
	device *dev=rt_device_find("spi_flash");
	if(dev==NULL)
		return;
	if(update_flashfont()==false)
		return;
	switch (fontsize)
    {
    	case 12:
			addr=show_zi_addr.GBK_12_addr;
    		break;
    	case 16:
			addr=show_zi_addr.GBK_16_addr;
    		break;
    	case 24:
			addr=show_zi_addr.GBK_24_addr;
    		break;
    }
	rt_device_read(dev,addr+foffset,buf,csize);
}

#define VER 0x2f77
uint8_t update_flashfont()
{
	char *filename;
	uint8_t *mem2base=0;
	uint8_t is_have[2]={0};
	uint32_t index=0;
	uint16_t ver=VER;
	int32_t read_len=0;
	uint32_t gbk_addr=0;
	uint32_t gbk_flash=0;
	device *dev=rt_device_find("spi_flash");
	rt_device_init(dev);
	if(dev!=NULL)
	{
		rt_device_control(dev,W25Q_READY,NULL);
	}
	else
		return false;	
	show_zi_addr.ascii_12_addr=15;
	show_zi_addr.ascii_16_addr=show_zi_addr.ascii_12_addr+sizeof(asc2_1206);
	show_zi_addr.ascii_24_addr=show_zi_addr.ascii_16_addr+sizeof(asc2_1608);
	show_zi_addr.ascii_32_addr=show_zi_addr.ascii_24_addr+sizeof(asc2_2412);
	
	rt_device_read(dev,2,(uint8_t*)&gbk_addr,4);
	show_zi_addr.GBK_12_addr=gbk_addr;
	rt_device_read(dev,6,(uint8_t*)&gbk_addr,4);
	show_zi_addr.GBK_16_addr=gbk_addr;
	rt_device_read(dev,10,(uint8_t*)&gbk_addr,4);
	show_zi_addr.GBK_24_addr=gbk_addr;
	show_zi_addr.size_zi=3125776;
	rt_device_read(dev,0,is_have,2);
	if((is_have[1]<<8|is_have[0])!=VER)
	{		
		rt_device_write(dev,0,(uint8_t*)&ver,2);
	}
	else		
		return true;
		
	
	if(file_init(0)!=FR_OK)
	{
		while(1)
		{
			
		}
	}
	for(int i=0;i<3;i++)
	{
		index=0;
		switch(i)
		{
			case 0:
				filename=GBK12_PATH;
				show_zi_addr.GBK_12_addr=show_zi_addr.ascii_32_addr+sizeof(asc2_3216)+2;
				gbk_flash=show_zi_addr.GBK_12_addr;
				show_zi_addr.size_zi=gbk_flash;
				break;
			case 1:
				filename=GBK16_PATH;
				show_zi_addr.GBK_16_addr=show_zi_addr.size_zi+2;
				gbk_flash=show_zi_addr.GBK_16_addr;
				break;
			case 2:
				filename=GBK24_PATH;
				show_zi_addr.GBK_24_addr=show_zi_addr.size_zi+2;
				gbk_flash=show_zi_addr.GBK_24_addr;
				break;
		}
		while(1)
		{
			mem2base=my_alloc(4096);
			if(file_lseek((uint8_t*)filename,mem2base,4096,index*4096,&read_len)==FR_OK)
			{
				if(read_len!=4096){
					rt_device_write(dev,gbk_flash,mem2base,read_len);
					show_zi_addr.size_zi+=read_len;
					index++;
					my_free(mem2base);
					break;
				}
				rt_device_write(dev,gbk_flash,mem2base,4096);
				gbk_flash+=read_len;
				show_zi_addr.size_zi+=read_len;
				index++;				
			}
			else
			{
				my_free(mem2base);
				break;
			}
		}
	}
	rt_device_write(dev,show_zi_addr.ascii_12_addr,(uint8_t*)asc2_1206,sizeof(asc2_1206));
	rt_device_write(dev,show_zi_addr.ascii_16_addr,(uint8_t*)asc2_1608,sizeof(asc2_1608));
	rt_device_write(dev,show_zi_addr.ascii_24_addr,(uint8_t*)asc2_2412,sizeof(asc2_2412));
	rt_device_write(dev,show_zi_addr.ascii_32_addr,(uint8_t*)asc2_3216,sizeof(asc2_3216));

	rt_device_write(dev,2,(uint8_t*)&show_zi_addr.GBK_12_addr,4);
	rt_device_write(dev,6,(uint8_t*)&show_zi_addr.GBK_16_addr,4);
	rt_device_write(dev,10,(uint8_t*)&show_zi_addr.GBK_24_addr,4);
	return true;
}







