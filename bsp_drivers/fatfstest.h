#ifndef __FATFSTEST_H
#define __FATFSTEST_H
#include "stm32f4xx_hal.h"
typedef enum{
	SD_FAT,
	FLASH_FAT,
}FAT_TYPE;


extern uint8_t file_init(uint8_t type);
extern uint8_t file_DeInit(uint8_t type);
extern uint8_t file_write(uint8_t *file_name,uint8_t *Write_Buffer,uint16_t Write_count);
extern uint8_t file_read(uint8_t *file_name,uint8_t *Read_Buffer,uint16_t Read_count,int32_t *readlen);
extern uint8_t Creat_NewFile(uint8_t *file_name);
extern uint8_t Delete_FileDirs(uint8_t *file_path);
extern uint8_t File_Mkdirs(uint8_t *dirs_name);
extern uint8_t file_lseek(uint8_t *file_name,uint8_t *Read_Buffer,uint16_t Read_count,uint32_t addr,int32_t *readlen);

#endif














