#include "bsp_crc32.h"
#include "drv_common.h"
CRC_HandleTypeDef hcrc;

void MX_CRC_Init(void)
{
  hcrc.Instance = CRC;
  if (HAL_CRC_Init(&hcrc) != HAL_OK)
  {
    Error_Handler();
  }
}

void HAL_CRC_MspInit(CRC_HandleTypeDef* hcrc)
{
  if(hcrc->Instance==CRC)
  {
    __HAL_RCC_CRC_CLK_ENABLE();
  }
}
void HAL_CRC_MspDeInit(CRC_HandleTypeDef* hcrc)
{
  if(hcrc->Instance==CRC)
  {
    __HAL_RCC_CRC_CLK_DISABLE();
  }
}

uint32_t CRC32(uint32_t pBuffer[], uint32_t BufferLength)
{
	return HAL_CRC_Accumulate(&hcrc,pBuffer,BufferLength);
}


















