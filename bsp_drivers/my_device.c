#include "device.h"
#ifdef MY_DEVICE
#include "config.h"

#define WAIT_SEM_LOCK_TIME	50
struct my_device obj_device={
	{
		"obj",
			0,
		{
			0,0
		}
	},
		0,
		0,
		0,
		0,
		0,
		0,
};
void device_lock_init(device *dev)
{
	dev->lock_sem= rt_sem_create(dev->parent.name,1,RT_IPC_FLAG_FIFO);
}
void device_lock(device *dev)
{
	rt_sem_take(dev->lock_sem,WAIT_SEM_LOCK_TIME);
}
void device_unlock(device *dev)
{
	rt_sem_release(dev->lock_sem);
}
int list_dev_init()
{
	list_init(&obj_device.parent.device_list);
	return true;
}
device *find_device(char *name)
{
	device *dev;
	dev=&obj_device;
	while(strcmp(name,dev->parent.name))
	{
		dev = (device*)rt_container_of(dev->parent.device_list.next,struct object,device_list);		
	}
	return dev;
}
int register_device(device *dev)
{
	if(dev!=NULL)
		list_insert_after(&obj_device.parent.device_list,&dev->parent.device_list);
	else return 0;
	return 1;
}
int delete_device(device *dev)
{
	if(dev!=NULL)
		list_remove(&dev->parent.device_list);
	else return 0;
	return 1;
}
int Get_device_num()
{
	return list_len(&obj_device.parent.device_list);
}
int device_init(device *dev)
{
	if(dev==NULL) return -1;
	if(dev->init_flag==true) return -1;
	
	device_lock_init(dev);
	if(dev->init(dev)==true)
	{
		dev->init_flag=true;//初始化完成
		return true;
	}
	else
		return false;
}
int device_open(device *dev)
{
	int sta;
	if(dev==NULL) return false;
	if(dev->init_flag==false)
	{
		if(dev->init(dev)==-1)
			return false;
	}
	sta= dev->open(dev);
	return sta;
}
int device_close(device *dev)
{
	int sta;
	if(dev==NULL) return false;
	dev->init_flag=false;
	sta=dev->close(dev);
	return sta;
}
int device_contorl(device *dev,uint8_t cmd,void *args)
{
	int sta;
	if(dev==NULL) return false;
	if(dev->init_flag==false)
	{
		if(dev->init(dev)==-1)
			return false;
	}
	sta=dev->contorl(dev,cmd,args);
	return sta;
}
int device_read(device *dev,int offest,uint8_t *buf,int len)
{
	int sta;
	if(dev==NULL) return false;
	if(dev->init_flag==false)
	{
		if(dev->init(dev)==-1)
			return false;
	}
	sta = dev->read(dev,offest,buf,len);
	return sta;
}
int device_write(device *dev,int offest,uint8_t *buf,int len)
{
	if(dev==NULL) return false;
	if(dev->init_flag==false)
	{
		if(dev->init(dev)==-1)
			return false;
	}
	return dev->write(dev,offest,buf,len);
}
#endif
int checkSystem()
{
	union check
	{
		int i;
		char ch;
	} c;
	c.i = 1;
	return(c.ch ==1);
}


	
/*命令函数段起始位置*/
int Device_Start(void)
{
	return 0;
}
DEVICE_START_EXPORT(Device_Start);
 
/*命令函数段结束位置*/
int Device_End(void)
{
	return 0;
}
DEVICE_END_EXPORT(Device_End);

int APP_end(void)
{
	return 0;
}
APP_END_EXPORT(APP_end);

void auto_init(void)
{
	const device_auto_init *cmd_ptr;
	for (cmd_ptr = &cmd_fn_Device_Start; cmd_ptr < &cmd_fn_Device_End;cmd_ptr++)
	{		
		(*cmd_ptr)();
	}
}
//extern int uart_thread(void);
extern int mem_init(void);
int device_AutoInit(void)
{
	auto_init();
//	uart_thread();
	mem_init();
//	uint32_t device_num=Get_device_num();
//	obj_device.parent.Dev_Num=device_num;
//	device *dev=&obj_device;
//	while(device_num--&&dev->parent.device_list.next!=NULL)
//	{
//		dev=(device*)rt_container_of(dev->parent.device_list.next,struct object,device_list);
//		device_init(dev);		
//	}
//	
	return 1;
}
INIT_APP_EXPORT(device_AutoInit);
int APP_Init()
{
	const device_auto_init *cmd_ptr;
	for (cmd_ptr = &cmd_fn_Device_End; cmd_ptr < &cmd_fn_APP_end;cmd_ptr++)
	{		
		(*cmd_ptr)();
	}
//	uint32_t device_num=Get_device_num();
//	obj_device.parent.Dev_Num=device_num;
//	device *dev=&obj_device;
//	while(device_num--&&dev->parent.device_list.next!=NULL)
//	{
//		dev=(device*)rt_container_of(dev->parent.device_list.next,struct object,device_list);
//		device_init(dev);		
//	}
	return 1;
}
INIT_APP_EXPORT(APP_Init);











