#include "bsp_uart.h"
#include "device.h"
#include "stdbool.h"
#include "drv_common.h"
#define DELAY_FRAME_TIMEOUT_UART1 100
#define DELAY_FRAME_TIMEOUT_UART2 300
#define DELAY_FRAME_TIMEOUT_UART3 300
#define device  struct rt_device
UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;

COM_PAR com1_par={0};
COM_PAR com2_par={0};
COM_PAR com3_par={0};
#define COM1_MAX 500
#define COM2_MAX 10
#define COM3_MAX 1200
uint8_t COM1_TxBUFFER[COM1_MAX]={0};
uint8_t COM1_RxBUFFER[COM1_MAX]={0};
uint8_t COM2_TxBUFFER[COM2_MAX]={0};
uint8_t COM2_RxBUFFER[COM2_MAX]={0};
uint8_t COM3_TxBUFFER[COM3_MAX]={0};
uint8_t COM3_RxBUFFER[COM3_MAX]={0};

COM_CONFIG com1cfg=com1_config;
COM_CONFIG com2cfg=com2_config;
COM_CONFIG com3cfg=com3_config;
COM_DEV COM1_DEV={0};
COM_DEV COM2_DEV={0};
COM_DEV COM3_DEV={0};
int uart_thread(void);


int MX_USART_UART_Init(COM_PAR *com_index,COM_CONFIG *com_config)
{	
	if(com_index->huart==&huart1)
	{
		com_index->huart->Instance = USART1; 
	}
	else if(com_index->huart==&huart2)
	{
		com_index->huart->Instance = USART2; 
	}
	else if(com_index->huart==&huart3)
	{
		com_index->huart->Instance = USART3; 
	}
	com_index->huart->Init.BaudRate = com_config->BaudRate;
	com_index->huart->Init.WordLength = com_config->WordLength;
	com_index->huart->Init.StopBits = com_config->StopBits;
	com_index->huart->Init.Parity = com_config->Parity;
	com_index->huart->Init.Mode = UART_MODE_TX_RX;
	com_index->huart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
	com_index->huart->Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(com_index->huart) != HAL_OK)
	{
		Error_Handler();	  
	}
	HAL_UART_Receive_IT(com_index->huart,&com_index->revc_data,1);
	return 0;
}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(uartHandle->Instance==USART1)
  {
    __HAL_RCC_USART1_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    GPIO_InitStruct.Pin = USART1_TX_Pin|USART1_RX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    HAL_NVIC_SetPriority(USART1_IRQn, 6, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
  }
  else if(uartHandle->Instance==USART2)
  {
    __HAL_RCC_USART2_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
	  
    GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART2_IRQn);
  }
  else if(uartHandle->Instance==USART3)
  {
    __HAL_RCC_USART3_CLK_ENABLE();
	//tx-PB10 RX-PB11
    __HAL_RCC_GPIOB_CLK_ENABLE();
    GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART3;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    HAL_NVIC_SetPriority(USART3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART3_IRQn);
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{
  if(uartHandle->Instance==USART1)
  {
    __HAL_RCC_USART1_CLK_DISABLE();
    HAL_GPIO_DeInit(GPIOA, USART1_TX_Pin|USART1_RX_Pin);
    HAL_NVIC_DisableIRQ(USART1_IRQn);
  }
  else  if(uartHandle->Instance==USART2)
  {
    __HAL_RCC_USART2_CLK_DISABLE();
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_2|GPIO_PIN_3);
    HAL_NVIC_DisableIRQ(USART2_IRQn);
  }
  else if(uartHandle->Instance==USART3)
  {
    __HAL_RCC_USART3_CLK_DISABLE();
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_10|GPIO_PIN_11);
    HAL_NVIC_DisableIRQ(USART3_IRQn);
  }
}
void rt_hw_console_output(const char *str)
{
	rt_enter_critical();
    rt_size_t i = 0, size = 0;
    char a = '\r';

    __HAL_UNLOCK(&huart1);

    size = rt_strlen(str);
    for (i = 0; i < size; i++)
    {
        if (*(str + i) == '\n')
        {
           HAL_UART_Transmit(&huart1,(uint8_t *)&a, 1, 1);
        }
		HAL_UART_Transmit(&huart1, (uint8_t *)(str + i), 1, 1);
    }
	rt_exit_critical();
}
char rt_hw_console_getchar(void)
{
	int8_t ch = -1;	
	device *uartdev=rt_device_find("uart1");
	rt_device_read(uartdev,0,(uint8_t*)&ch,1);//错误返回-1
	return ch;  
}
int Get_queue_len(COM_PAR *com_index,uint8_t type)
{
	if(type==tx)
		return (com_index->max+com_index->ring_TXbuf.tail-com_index->ring_TXbuf.head)%com_index->max;
	else if(type==rx)
		return (com_index->max+com_index->ring_RXbuf.tail-com_index->ring_RXbuf.head)%com_index->max;
	return false;
}
bool isEmpty_queue(COM_PAR *com_index,uint8_t type)
{
	if(type==tx)
		return (com_index->ring_TXbuf.tail==com_index->ring_TXbuf.head);
	else if(type==rx)
		return (com_index->ring_RXbuf.tail==com_index->ring_RXbuf.head);
	return true;
}
bool is_fill_queue(COM_PAR *com_index,uint8_t type)
{
	if(type==tx)
		return ((com_index->ring_TXbuf.tail+1)%com_index->max==com_index->ring_TXbuf.head);
	else if(type==rx)
		return ((com_index->ring_RXbuf.tail+1)%com_index->max==com_index->ring_RXbuf.head);
	return true;
}
int push_queue(COM_PAR *com_index,uint8_t type,uint8_t element)
{
	if(type==tx)
	{
		if(is_fill_queue(com_index,type)==true) return false;		
		com_index->ring_TXbuf.buf[com_index->ring_TXbuf.tail]=element;
		com_index->ring_TXbuf.tail=(com_index->ring_TXbuf.tail+1)%com_index->max;
		return true;		
	}	
	else if(type==rx)
	{
		if(is_fill_queue(com_index,type)==true) return false;		
		com_index->ring_RXbuf.buf[com_index->ring_RXbuf.tail]=element;
		com_index->ring_RXbuf.tail=(com_index->ring_RXbuf.tail+1)%com_index->max;
		return true;		
	}
	return false;
}
int pop_queue(COM_PAR *com_index,uint8_t type)
{
	uint8_t tmp=0;
	if(type==tx)
	{
		if(isEmpty_queue(com_index,type)==true) return -1;
		tmp=com_index->ring_TXbuf.buf[com_index->ring_TXbuf.head];
		com_index->ring_TXbuf.head=(com_index->ring_TXbuf.head+1)%com_index->max;
		return tmp;
	}
	else if(type==rx)
	{
		if(isEmpty_queue(com_index,type)==true) return -1;
		tmp=com_index->ring_RXbuf.buf[com_index->ring_RXbuf.head];
		com_index->ring_RXbuf.head=(com_index->ring_RXbuf.head+1)%com_index->max;
		return tmp;
	}
	return -1;
}
int queue_read(COM_PAR *com_index,uint8_t type,uint8_t *buf,int len)
{
	int i=0,j=0;
	if(isEmpty_queue(com_index,type)==true) return 0;//是空的返回-1
	j=Get_queue_len(com_index,type);
	if(j<len) len=j;
	for(i=0;i<len;i++)
	{
		buf[i]=pop_queue(com_index,type);
	}
	return len;
}
int queue_write(COM_PAR *com_index,uint8_t type,uint8_t *buf,int len)
{
	int i=0;
	for(i=0;i<len;i++)
	{
		push_queue(com_index,type,buf[i]);
	}
	return len;
}
/* 定时器 1 超时函数 */
static void timeout1(void *parameter)
{
	COM_PAR *com_index=0;
	com_index=(COM_PAR*)parameter;
    com_index->COM_frame_flag=1;
	com_index->rx_len=Get_queue_len(com_index,rx);
}
static rt_thread_t tid1 = RT_NULL;

static void thread1_entry(void *parameter)
{
	int len=0;
	uint8_t uart_buf=0;
    while (1)
    {
		len=Get_queue_len(&com2_par,tx);
        if(len!=0&&com2_par.tx_flag==0) 
		{		
			com2_par.tx_flag=1;				
			queue_read(&com2_par,tx,&uart_buf,1);			
			HAL_UART_Transmit_IT(com2_par.huart,&uart_buf,1);
		}
		len=Get_queue_len(&com3_par,tx);
        if(len!=0&&com3_par.tx_flag==0) 
		{		
			com3_par.tx_flag=1;				
			queue_read(&com3_par,tx,&uart_buf,1);			
			HAL_UART_Transmit_IT(com3_par.huart,&uart_buf,1);
		}
        rt_thread_delay(3);
//		rt_thread_yield();
    }
}
int uart_thread(void)
{
	tid1 = rt_thread_create("thread1",
                            thread1_entry, RT_NULL,
                            512,
                            10, 10);

    /* 如果获得线程控制块，启动这个线程 */
    if (tid1 != RT_NULL)
        rt_thread_startup(tid1);
    /* 创建定时器 1  周期定时器 */
    com1_par.timer = rt_timer_create("uart1_t", timeout1,
                             &com1_par, DELAY_FRAME_TIMEOUT_UART1,
                             RT_TIMER_FLAG_ONE_SHOT);//?

    /* 创建定时器 2 单次定时器 */
    com2_par.timer = rt_timer_create("uart2_t", timeout1,
                             &com2_par,  DELAY_FRAME_TIMEOUT_UART2,
                             RT_TIMER_FLAG_ONE_SHOT);
	com3_par.timer = rt_timer_create("uart3_t", timeout1,
                             &com3_par,  DELAY_FRAME_TIMEOUT_UART3,
                             RT_TIMER_FLAG_ONE_SHOT);
    return 0;
}

void USART1_IRQHandler(void)
{
    /* enter interrupt */
    rt_interrupt_enter();

    HAL_UART_IRQHandler(&huart1);

    /* leave interrupt */
    rt_interrupt_leave();
}
void USART2_IRQHandler(void)
{
	/* enter interrupt */
	rt_interrupt_enter();

	HAL_UART_IRQHandler(&huart2);

	/* leave interrupt */
	rt_interrupt_leave();
}
void USART3_IRQHandler(void)
{
	/* enter interrupt */
	rt_interrupt_enter();

	HAL_UART_IRQHandler(&huart3);

	/* leave interrupt */
	rt_interrupt_leave();
}
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	COM_DEV *Com_dev;
	if(huart==COM1_DEV.com_par->huart)
		Com_dev=&COM1_DEV;
	if(huart==COM2_DEV.com_par->huart)
		Com_dev=&COM2_DEV;
	if(huart==COM3_DEV.com_par->huart)
		Com_dev=&COM3_DEV;

	Com_dev->com_par->tx_flag=0;
}
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	COM_DEV *Com_dev;
	
	if(huart==COM1_DEV.com_par->huart)
		Com_dev=&COM1_DEV;
	if(huart==COM2_DEV.com_par->huart)
		Com_dev=&COM2_DEV;
	if(huart==COM3_DEV.com_par->huart)
		Com_dev=&COM3_DEV;
	
	Com_dev->com_par->COM_frame_flag=0;
	rt_timer_start(Com_dev->com_par->timer);			
	queue_write(Com_dev->com_par,rx,&Com_dev->com_par->revc_data,1);
	HAL_UART_Receive_IT(Com_dev->com_par->huart,&Com_dev->com_par->revc_data,1);
	Com_dev->dev.rx_indicate(&Com_dev->dev,1);	
}



int Open_Com_channel(COM_DEV *dev)
{
	dev->com_par->COM_Occupy=true;
	return dev->com_par->COM_Occupy;
}
int Close_Com_channel(COM_DEV *dev)
{	
//	HAL_UART_MspDeInit(com_index.huart);
	dev->com_par->COM_Occupy=false;		
	dev->com_par->COM_frame_flag=false;	
	return RT_EOK;
}

int Get_Com_Occupy(COM_DEV *dev)
{
	return	dev->com_par->COM_Occupy;
}
int Get_uart_Frame(COM_DEV *dev)
{
	if(dev->com_par->COM_frame_flag==1) 
	{	
		return true;
	}
	return false;
}
uint32_t Get_uart_RxLen(COM_DEV *dev)
{
	uint32_t len=Get_queue_len(dev->com_par,rx);
	dev->com_par->rx_len=len;
	if(len==0)
		dev->com_par->COM_frame_flag=0;
	return len;
}
uint32_t Get_uart_TxLen(COM_DEV *dev)
{
	uint32_t len=Get_queue_len(dev->com_par,tx);
	return len;
}
int uartDev_contorl(COM_DEV *dev,uint8_t cmd,void *args)
{
	COM_CONFIG *com_config;
	switch(cmd)
	{
		case GET_OCCUPY_FLAG:
			*(int*)args=Get_Com_Occupy(dev);
		break;
		case SET_SET_PAR:
			HAL_UART_MspDeInit(dev->com_par->huart);
			com_config=(COM_CONFIG *)args;
			dev->com_par->huart->Init.BaudRate = com_config->BaudRate;
			dev->com_par->huart->Init.WordLength = com_config->WordLength;
			dev->com_par->huart->Init.StopBits = com_config->StopBits;
			dev->com_par->huart->Init.Parity = com_config->Parity;
			dev->com_par->huart->Init.Mode = UART_MODE_TX_RX;
			dev->com_par->huart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
			dev->com_par->huart->Init.OverSampling = UART_OVERSAMPLING_16;
			if (HAL_UART_Init(dev->com_par->huart) != HAL_OK)
			{
				Error_Handler();	  
			}
			HAL_UART_Receive_IT(dev->com_par->huart,&com1_par.revc_data,1);
		break;
		case CLOSE_COM:
			HAL_UART_MspDeInit(dev->com_par->huart);
		break;
		case OPEN_COM:
			com_config=(COM_CONFIG *)args;
			if(dev->com_par->huart==&huart1)
			{
				dev->com_par->huart->Instance = USART1; 
			}
			else if(dev->com_par->huart==&huart2)
			{
				dev->com_par->huart->Instance = USART2; 
			}				
			dev->com_par->huart->Init.BaudRate = com_config->BaudRate;
			dev->com_par->huart->Init.WordLength = com_config->WordLength;
			dev->com_par->huart->Init.StopBits = com_config->StopBits;
			dev->com_par->huart->Init.Parity = com_config->Parity;
			dev->com_par->huart->Init.Mode = UART_MODE_TX_RX;
			dev->com_par->huart->Init.HwFlowCtl = UART_HWCONTROL_NONE;
			dev->com_par->huart->Init.OverSampling = UART_OVERSAMPLING_16;
			if (HAL_UART_Init(dev->com_par->huart) != HAL_OK)
			{
				Error_Handler();	  
			}
			HAL_UART_Receive_IT(dev->com_par->huart,&com1_par.revc_data,1);
		break;
		case GET_FRAM_FLAG:
			*(uint8_t*)args= Get_uart_Frame(dev);
			break;
		case GET_RX_LEN:
			*(uint32_t*)args= Get_uart_RxLen(dev);
			break;
	}
	return RT_EOK;
}

rt_err_t uart_init(device *Device)
{	
	COM_DEV* com_dev;
	com_dev = (COM_DEV*)rt_container_of(Device,struct __COM_DEV,dev);
	MX_USART_UART_Init(com_dev->com_par,&com_dev->com_par->comfig);
	return RT_EOK;
}
rt_err_t uart_open(device *Device, rt_uint16_t oflag)
{
//	device_lock(Device);
	COM_DEV* com_dev;
	com_dev = (COM_DEV*)rt_container_of(Device,struct __COM_DEV,dev);
	Open_Com_channel(com_dev);
//	device_unlock(Device);
	return RT_EOK;
}
rt_err_t uart_close(device *Device)
{
//	device_lock(Device);
	COM_DEV* com_dev;
	com_dev = (COM_DEV*)rt_container_of(Device,struct __COM_DEV,dev);
	Close_Com_channel(com_dev);
//	device_unlock(Device);
	return RT_EOK;
}
rt_size_t uart_read(device *Device,rt_off_t offest,void *buf,rt_size_t len)
{
	int state;
	uint16_t rx_len=0;
	COM_DEV* com_dev;
	com_dev = (COM_DEV*)rt_container_of(Device,struct __COM_DEV,dev);	
	rt_device_control(Device,GET_RX_LEN,&rx_len);
	if(rx_len==0) 
	{
		com_dev->com_par->COM_frame_flag=0;
		*(uint8_t*)buf=0xff;
		return 0;
	}
//	device_lock(Device);
	state = queue_read(com_dev->com_par,rx,buf,len);
	rt_device_control(Device,GET_RX_LEN,&rx_len);
	if(rx_len==0)
		com_dev->com_par->COM_frame_flag=0;
//	device_unlock(Device);
	return state;
}

rt_size_t uart_write(device *Device,rt_off_t offest,const void  *buf,rt_size_t len)
{
//	device_lock(Device);
	
	COM_DEV* com_dev; 
	com_dev = (COM_DEV*)rt_container_of(Device,struct __COM_DEV,dev);
	if(strcmp(Device->parent.name,"uart1")==NULL)
	{
		 if(HAL_UART_Transmit(com_dev->com_par->huart,(uint8_t *)buf, len, 300)==HAL_OK)
			return len;
		 else
			return 0;
	}
	int state = queue_write(com_dev->com_par,tx,(uint8_t*)buf,len);
	
	return state;
}
rt_err_t uart_contorl(device *Device,int cmd,void *args)
{
//	device_lock(Device);
	COM_DEV* com_dev;
	com_dev = (COM_DEV*)rt_container_of(Device,struct __COM_DEV,dev);	
	int state = uartDev_contorl(com_dev,cmd,args);
//	device_unlock(Device);
	return state;
}


int register_uart()
{
//	COM1_DEV.dev.parent.name="uart1";

	memset(&com1_par,0,sizeof(COM_PAR));
	com1_par.huart=&huart1;
	com1_par.max=COM1_MAX;
	com1_par.ring_RXbuf.buf=COM1_RxBUFFER;
	com1_par.ring_RXbuf.head=0;
	com1_par.ring_RXbuf.tail=0;
	
	com1_par.ring_TXbuf.buf=COM1_TxBUFFER;
	com1_par.ring_TXbuf.head=0;
	com1_par.ring_TXbuf.tail=0;
	
	com1_par.comfig=com1cfg;
	
	COM1_DEV.dev.init=uart_init;
	COM1_DEV.dev.open=uart_open;
	COM1_DEV.dev.close=uart_close;
	COM1_DEV.dev.read=uart_read;
	COM1_DEV.dev.write=uart_write;
	COM1_DEV.dev.control=uart_contorl;
	COM1_DEV.com_par=&com1_par;	
	
	rt_device_register(&COM1_DEV.dev,"uart1",RT_DEVICE_FLAG_RDWR); 
	
//	COM2_DEV.dev.parent.name="uart2";

	memset(&com2_par,0,sizeof(COM_PAR));
	com2_par.huart=&huart2;
	com2_par.max=COM2_MAX;
	
	com2_par.ring_RXbuf.buf=COM2_RxBUFFER;
	com2_par.ring_RXbuf.head=0;
	com2_par.ring_RXbuf.tail=0;
	com2_par.ring_TXbuf.buf=COM2_TxBUFFER;
	com2_par.ring_TXbuf.head=0;
	com2_par.ring_TXbuf.tail=0;
	
	com2_par.comfig=com2cfg;	
	
	COM2_DEV.dev.init=uart_init;
	COM2_DEV.dev.open=uart_open;
	COM2_DEV.dev.close=uart_close;
	COM2_DEV.dev.read=uart_read;
	COM2_DEV.dev.write=uart_write;
	COM2_DEV.dev.control=uart_contorl;
	COM2_DEV.com_par=&com2_par;	
	rt_device_register(&COM2_DEV.dev,"uart2",RT_DEVICE_FLAG_RDWR);
	
//	COM3_DEV.dev.parent.name="uart3";

	memset(&com3_par,0,sizeof(COM_PAR));
	com3_par.huart=&huart3;
	com3_par.max=COM3_MAX;
	com3_par.ring_RXbuf.buf=COM3_RxBUFFER;
	com3_par.ring_RXbuf.head=0;
	com3_par.ring_RXbuf.tail=0;
	com3_par.ring_TXbuf.buf=COM3_TxBUFFER;
	com3_par.ring_TXbuf.head=0;
	com3_par.ring_TXbuf.tail=0;
	com3_par.comfig=com3cfg;
	
	COM3_DEV.dev.init=uart_init;
	COM3_DEV.dev.open=uart_open;
	COM3_DEV.dev.close=uart_close;
	COM3_DEV.dev.read=uart_read;
	COM3_DEV.dev.write=uart_write;
	COM3_DEV.dev.control=uart_contorl;
	COM3_DEV.com_par=&com3_par;	
	rt_device_register(&COM3_DEV.dev,"uart3",RT_DEVICE_FLAG_RDWR);
	return true;
}

