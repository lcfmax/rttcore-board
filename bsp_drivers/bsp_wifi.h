#ifndef __BSP_WIFI_H
#define __BSP_WIFI_H
#include "config.h"




#define RST 	PIN_IN(A,1)
#define IO_0 	PIN_OUT(A,7)


typedef struct {
    char 		*cmd;
    char 		*true_ack;
    char 		*err_ack;
    uint32_t 	timeout;//超过设定时间收不到数据则直接结束
    uint8_t		retry_times;//收到错误字符，重试次数
} ESP8266_TABLE;

typedef struct {
    char 		APIP[30];
    char 		STAIP[30];
} ESP8266_LOCATED_IP;//本机ip地址

typedef struct {
	uint8_t     ready_read;//可以读取数据
   struct       rt_ringbuffer *rx_ringbuffer;
	uint16_t 	recv_len;		//接收的数据长度
} ESP8266_RECV_DATA;//接收的数据

typedef struct
{
	char		connect_device_index;//
    char  		type[5];//tcp，udp
    char  		ip[30];
    char 		remote_port[20];//远程端口
    char 		local_port[20];//本地端口
    char 		connect_type;//0-客户端 1-服务端
} CONNECT_DEVICE_INFO;//已连接设备的信息

#define CONNECT_DEVICE_MAX 5
#define WIFI_RINGBUFFER_MAX 200
typedef struct {
    struct 	rt_device 	dev;								//wifi设备
    rt_device_t 		uart_dev;							//wifi所用串口设备
	
    rt_thread_t 		Esp_tx_tid;							//wifi发送线程
    rt_thread_t 		Esp_rx_tid;							//wifi接收线程
    rt_sem_t    		tx_sem;								//wifi发送数据信号量
	rt_sem_t			read_sem;							//wifi读取数据信号量
    rt_timer_t  		Esp_timeOut_timer;					//wifi初始化过程无数据接收超时定时器
    uint8_t 			timeout_flag;						//wifi超时标志
    uint8_t     		AT_index;							//wifi AT命令状态表索引
    uint8_t 			*read_buf;							//wifi接收数据指针
    uint8_t				step;								//wifi初始化步骤 0-发送命令 1-等待回应 2-初始化完成，接收数据 3-初始化失败
	uint8_t				*tx_data_point;						//wifi发送数据指针
	uint8_t				tx_data_len;						//wifi发送的数据长度
	uint8_t 			tx_finish_flag;						//wifi发送数据成功标志
    ESP8266_LOCATED_IP 	located_ip;							//wifi本机ip地址
    ESP8266_RECV_DATA	esp8266_recv_data[CONNECT_DEVICE_MAX];					//wifi接收数据结构体
	uint8_t				esp8266_work_mode;
    CONNECT_DEVICE_INFO Connect_Device_Info[CONNECT_DEVICE_MAX];	//wifi连接的设备信息
	ESP8266_TABLE 		*esp8266_table;						//wifi初始化状态表
} DEV_ESP8266;

#define GET_WIFI_RX_LEN		0
#define GET_WIFI_TX_STATE	1
#define REGISTER_WIFI_FUN	2
#define QUERY_CONNECT_IP_PORT	3
#endif










