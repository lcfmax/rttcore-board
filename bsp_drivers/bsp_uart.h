#ifndef __BSP_UART_H
#define __BSP_UART_H
#include "stm32f4xx_hal.h"
#include "rtthread.h"
#include "device.h"
extern RTC_HandleTypeDef hrtc;
extern UART_HandleTypeDef huart1;

#define USART1_TX_Pin GPIO_PIN_9
#define USART1_TX_GPIO_Port GPIOA
#define USART1_RX_Pin GPIO_PIN_10
#define USART1_RX_GPIO_Port GPIOA


__packed struct Circular_queue{
	int head;//指向队头元素
	int tail;//指向队尾元素
	uint8_t *buf;//指针指向缓冲区
};
typedef enum{
	COM1,
	COM2,
}COM_TYPE;
typedef enum{
	tx,
	rx,
}txrx_TYPE;

__packed typedef struct {
	uint32_t BaudRate;
	uint32_t Parity;
	uint32_t WordLength; 
	uint32_t StopBits; 	 
}COM_CONFIG;

__packed typedef struct
{	
	UART_HandleTypeDef *huart;
	rt_timer_t timer;
	uint8_t revc_data;
	uint8_t COM_frame_flag;
	uint8_t COM_Occupy;
	struct Circular_queue ring_TXbuf;
	struct Circular_queue ring_RXbuf;
	int max;
	uint32_t rx_len;
	uint8_t tx_flag;
	COM_CONFIG comfig;
}COM_PAR;
struct __COM_DEV{
	struct rt_device dev;
	COM_PAR *com_par;
};
typedef struct __COM_DEV COM_DEV;





#define com1_config \
{ \
	115200, \
	UART_PARITY_NONE, \
	UART_WORDLENGTH_8B, \
	UART_STOPBITS_1, \
}
#define com2_config \
{ \
	115200, \
	UART_PARITY_NONE, \
	UART_WORDLENGTH_8B, \
	UART_STOPBITS_1, \
}
#define com3_config \
{ \
	115200, \
	UART_PARITY_NONE, \
	UART_WORDLENGTH_8B, \
	UART_STOPBITS_1, \
}
#define GET_OCCUPY_FLAG 0  //获取串口占用标志位
#define SET_SET_PAR   	1  //设置参数
#define CLOSE_COM		2  //关闭串口外设
#define OPEN_COM		3  //打开外设
#define GET_FRAM_FLAG	4  //获取成帧标志
#define GET_RX_LEN		5  //获取接收长度
extern int uart_thread(void);



#endif







