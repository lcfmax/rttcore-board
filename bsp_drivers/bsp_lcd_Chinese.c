#include "bsp_lcd.h"
#include "config.h"

extern void Read_Font(uint8_t *buf,uint8_t fontsize,uint8_t *HZ);

addr_hz show_zi_addr;
uint8_t lcd_buf[80]={0};
void LCD_ShowOneChinese(uint16_t x,uint16_t y,uint8_t size,uint8_t *p,uint16_t color)
{   	
	uint8_t temp,t1,t;
	uint16_t y0=y;

	uint8_t csize=(size/8+((size%8)?1:0))*(size);		//得到字体一个字符对应点阵集所占的字节数		
	Read_Font(lcd_buf,size,p);
	uint32_t GBK_addr=(uint32_t)&lcd_buf;
	for(t=0;t<csize;t++)
	{   

		temp=*(uint8_t*)GBK_addr++;
								
		for(t1=0;t1<8;t1++)
		{			    
			if(temp&0x80)LCD_DrawPoint(x,y,color);
			temp<<=1;
			y++;
			if(y>=lcd_dev.height)return;		//超区域了
			if((y-y0)==size)
			{
				y=y0;
				x++;
				if(x>=lcd_dev.width)return;	//超区域了
				break;
			}
		}  	 
	}  
}

void LCD_ShowChineseString(uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t size,uint8_t *p,uint16_t color)
{         
	uint8_t x0=x;
	width+=x;
	height+=y;
	uint8_t ch_H=*p;
	uint8_t ch_L=*(p+1);
    while((ch_H<=0xFE)&&(ch_L>=0x40))//判断是不是非法字符!
    {    		
        if(x>=width){x=x0;y+=size;}
        if(y>=height)break;//退出
        LCD_ShowOneChinese(x,y,size,p,color);
        x+=size;
		p+=2;
        ch_H=*p;
		ch_L=*(p+1);
    }  
}

void LCD_ShowAll(uint16_t x,uint16_t y,uint16_t width,uint16_t height,uint8_t size,uint8_t *p,uint16_t color)
{
	uint8_t c[2]={0};
	int i;
	uint16_t x0=x,y0=y;
	uint16_t w=0,h=0;
	w=width+x;
	h=height+y;
	for(i=0;i<strlen((char*)p);i++)
	{
		if(p[i]<128)
		{
			if(x0>=w){x0=x;y0+=size;}
			if(y0>h) return;
			c[0]=p[i];
			LCD_ShowString(x0,y0,width,height,size,c,color);
			x0+=size/2;				
		}		
		else
		{
			if(x0>=w){x0=x;y0+=size;}
			if(y0>h) return;
			c[0]=p[i];
			c[1]=p[i+1];
			i+=1;
			LCD_ShowChineseString(x0,y0,width,height,size,c,color);	
			x0+=size;		
		}
	}
}





















