#include "bsp_touch.h"
#include "bsp_lcd.h"
#include "soft_spi.h"

_m_tp_dev tp_dev=
{
	TP_Init,
	TP_Scan,
	0,
	0,
	0, 
	0,
	0,
	0,
	0,	  	 		
	0,
	0,	  	 		
};	

uint8_t CMD_RDX=0XD0;
uint8_t CMD_RDY=0X90;

//SPI写数据
//向触摸屏IC写入1byte数据    
//num:要写入的数据
void TP_Write_Byte(uint8_t num)    
{  
	uint8_t count=0;   
	for(count=0;count<8;count++)  
	{ 	  
		if(num&0x80)TDIN=1;  
		else TDIN=0;   
		num<<=1;    
		TCLK=0; 
		delay_us(1);
		TCLK=1;		//上升沿有效	        
	}		 			    
} 		 
//SPI读数据 
//从触摸屏IC读取adc值
//CMD:指令
//返回值:读到的数据	   
uint16_t TP_Read_AD(uint8_t CMD)	  
{ 	 
	uint8_t count=0; 	  
	uint16_t Num=0;
	TCLK=0;		//先拉低时钟 	 
	TDIN=0; 	//拉低数据线
	 TCS=0; 		//选中触摸屏IC
	TP_Write_Byte(CMD);//发送命令字
	TCLK=0; 	     	    
	delay_us(1);    	   
	TCLK=1;		//给1个时钟，清除BUSY
	delay_us(1);    
	TCLK=0; 	     	    
	for(count=0;count<16;count++)//读出16位数据,只有高12位有效 
	{ 				  
		Num<<=1; 	 
		TCLK=0;	//下降沿有效  	    	   
		delay_us(1);    
 		TCLK=1;
 		if(DOUT)Num++; 		 
	}  	
	Num>>=4;   	//只有高12位有效.
	TCS=1;		//释放片选	 
	return(Num);   
}
#define READ_TIMES 5 	//读取次数
#define LOST_VAL 1	  	//丢弃值
//读取一个坐标值(x或者y)
//连续读取READ_TIMES次数据,对这些数据升序排列,
//然后去掉最低和最高LOST_VAL个数,取平均值 
//xy:指令（CMD_RDX/CMD_RDY）
//返回值:读到的数据
uint16_t buf1[READ_TIMES];
uint16_t TP_Read_XOY(uint8_t xy)
{
	uint16_t i, j;
	uint16_t sum=0;
	uint16_t temp;
	
	for(i=0;i<READ_TIMES;i++)buf1[i]=SPI_Send(xy);		 		    
	for(i=0;i<READ_TIMES-1; i++)//排序
	{
		for(j=i+1;j<READ_TIMES;j++)
		{
			if(buf1[i]>buf1[j])//升序排列
			{
				temp=buf1[i];
				buf1[i]=buf1[j];
				buf1[j]=temp;
			}
		}
	}	  
	sum=0;
	
	for(i=LOST_VAL;i<READ_TIMES-LOST_VAL;i++)sum+=buf1[i];
	temp=sum/(READ_TIMES-2*LOST_VAL);
//	memset(buf1,0,sizeof(buf1));
	return temp;   
} 
//读取x,y坐标
//最小值不能少于100.
//x,y:读取到的坐标值
//返回值:0,失败;1,成功。
uint8_t TP_Read_XY(uint16_t *x,uint16_t *y)
{
	uint16_t xtemp,ytemp;			 	 		  
	xtemp=TP_Read_XOY(CMD_RDX);
	ytemp=TP_Read_XOY(CMD_RDY);	  												   
	//if(xtemp<100||ytemp<100)return 0;//读数失败
	*x=xtemp;
	*y=ytemp;
	return 1;//读数成功
}
//连续2次读取触摸屏IC,且这两次的偏差不能超过
//ERR_RANGE,满足条件,则认为读数正确,否则读数错误.	   
//该函数能大大提高准确度
//x,y:读取到的坐标值
//返回值:0,失败;1,成功。
#define ERR_RANGE 50 //误差范围 
uint8_t TP_Read_XY2(uint16_t *x,uint16_t *y) 
{
	uint16_t x1,y1;
 	uint16_t x2,y2;
 	uint8_t flag;    
    flag=TP_Read_XY(&x1,&y1);   
    if(flag==0)return(0);
    flag=TP_Read_XY(&x2,&y2);	   
    if(flag==0)return(0);   
    if(((x2<=x1&&x1<x2+ERR_RANGE)||(x1<=x2&&x2<x1+ERR_RANGE))//前后两次采样在+-50内
    &&((y2<=y1&&y1<y2+ERR_RANGE)||(y1<=y2&&y2<y1+ERR_RANGE)))
    {
        *x=(x1+x2)/2;
        *y=(y1+y2)/2;
        return 1;
    }else return 0;	  
}  
//////////////////////////////////////////////////////////////////////////////////		  
//与LCD部分有关的函数  
//画一个触摸点
//用来校准用的
//x,y:坐标
//color:颜色
void TP_Drow_Touch_Point(uint16_t x,uint16_t y,uint16_t color)
{
	
	LCD_DrawLine(x-12,y,x+13,y,color);//横线
	LCD_DrawLine(x,y-12,x,y+13,color);//竖线
	LCD_DrawPoint(x+1,y+1,color);
	LCD_DrawPoint(x-1,y+1,color);
	LCD_DrawPoint(x+1,y-1,color);
	LCD_DrawPoint(x-1,y-1,color);
	LCD_Draw_Circle(x,y,6,color);//画中心圈
}	  
//画一个大点(2*2的点)		   
//x,y:坐标
//color:颜色
void TP_Draw_Big_Point(uint16_t x,uint16_t y,uint16_t color)
{	    
	LCD_DrawPoint(x,y,color);//中心点 
	LCD_DrawPoint(x+1,y,color);
	LCD_DrawPoint(x,y+1,color);
	LCD_DrawPoint(x+1,y+1,color);	 	  	
}						  
//////////////////////////////////////////////////////////////////////////////////		  
//触摸按键扫描
//tp:0,屏幕坐标;1,物理坐标(校准等特殊场合用)
//返回值:当前触屏状态.
//0,触屏无触摸;1,触屏有触摸
uint8_t TP_Scan(uint8_t tp)
{			   
	if(PEN==0)//有按键按下
	{

			if(tp)TP_Read_XY2(&tp_dev.x[0],&tp_dev.y[0]);//读取物理坐标
			else if(TP_Read_XY2(&tp_dev.x[0],&tp_dev.y[0]))//读取屏幕坐标
			{
				tp_dev.x[0]=tp_dev.xfac*tp_dev.x[0]+tp_dev.xoff;//将结果转换为屏幕坐标
				tp_dev.y[0]=tp_dev.yfac*tp_dev.y[0]+tp_dev.yoff;  
				tp_dev.sta|=TP_PRES_DOWN;
			} 
//		if((tp_dev.sta&TP_PRES_DOWN)==0)//之前没有被按下
//		{		 
//			tp_dev.sta=TP_PRES_DOWN|TP_CATH_PRES;//按键按下  
//			tp_dev.x[4]=tp_dev.x[0];//记录第一次按下时的坐标
//			tp_dev.y[4]=tp_dev.y[0];  	   			 
//		}	
		
				   
	}else
	{
		if(tp_dev.sta&TP_PRES_DOWN)//之前是被按下的
		{
			tp_dev.sta&=0x7f;//标记按键松开	
		}
//		else//之前就没有被按下
//		{
//			tp_dev.x[4]=0;
//			tp_dev.y[4]=0;
//			tp_dev.x[0]=0xffff;
//			tp_dev.y[0]=0xffff;
//		}	    
	}
	return tp_dev.sta&TP_PRES_DOWN;//返回当前的触屏状态
}	

//触摸屏初始化  		    
//返回值:0,没有进行校准
//       1,进行过校准
uint8_t TP_Init(void)
{
	GPIO_InitTypeDef GPIO_Initure;
	{
		__HAL_RCC_GPIOB_CLK_ENABLE();			//开启GPIOB时钟
		__HAL_RCC_GPIOC_CLK_ENABLE();			//开启GPIOC时钟
		__HAL_RCC_GPIOF_CLK_ENABLE();			//开启GPIOF时钟
		
		//GPIOB1,2初始化设置
		GPIO_Initure.Pin=GPIO_PIN_1|GPIO_PIN_2;	//PB1/PB2 设置为上拉输入
		GPIO_Initure.Mode=GPIO_MODE_INPUT;  	//输入模式
		GPIO_Initure.Pull=GPIO_PULLUP;          //上拉
		GPIO_Initure.Speed=GPIO_SPEED_HIGH;     //高速
		HAL_GPIO_Init(GPIOB,&GPIO_Initure);     //初始化
        
		//PB0
		GPIO_Initure.Pin=GPIO_PIN_0; 			//PB0设置为推挽输出
		GPIO_Initure.Mode=GPIO_MODE_OUTPUT_PP;  //推挽输出
		HAL_GPIO_Init(GPIOB,&GPIO_Initure);     //初始化
        
		//PC13
		GPIO_Initure.Pin=GPIO_PIN_13;          	//PC13设置为推挽输出
		HAL_GPIO_Init(GPIOC,&GPIO_Initure);     //初始化
        
		//PF11
		GPIO_Initure.Pin=GPIO_PIN_11;          	//PF11设置推挽输出
		HAL_GPIO_Init(GPIOF,&GPIO_Initure);     //初始化			
		xpt_2046_init();
		TP_Read_XY(&tp_dev.x[0],&tp_dev.y[0]);//第一次读取初始化	 
		LCD_Clear(WHITE);//清屏 
		tp_dev.xfac=0.0667779595;
		tp_dev.xoff=0xFFE6;
		tp_dev.yfac=0.0848999396;
		tp_dev.yoff=0xFFEF;
	}
	lcd_dev.tp_dev=&tp_dev;
	return 1; 									 
}










