#ifndef __THREAD_TIMER_MANAGER_H
#define __THREAD_TIMER_MANAGER_H
#include "rtthread.h"


typedef enum
{	
	Ctrl_Thread_Ctreate,
	Ctrl_Thread_Start,
	Ctrl_Thread_Delete,
	Ctrl_Thread_Suspend,
	Ctrl_Thread_Resume,
	Ctrl_Thread_Yield,
	Ctrl_Thread_GetCurrentThread,
	Ctrl_Thread_Sleep,
	Ctrl_Thread_Control,
	Ctrl_Thread_SetIdleHook,
	Ctrl_Thread_DelIdleHook,
	Ctrl_Thread_SetschedulerHook,
}THREAD_CTRL_TYPE;

typedef enum
{	
	Ctrl_Timer_Ctreate,
	Ctrl_Timer_Start,
	Ctrl_Timer_Stop,
	Ctrl_Timer_Delete,
	Ctrl_Timer_Control,
}TIMER_CTRL_TYPE;

typedef struct 
{
	const char* name;
	void (*entry)(void *parameter);
	void *parameter;
	uint16_t stack_size;
	uint8_t priority;
	uint32_t tick;
	rt_thread_t *GetCurrentThread;
	uint16_t delay;
	rt_uint8_t cmd;
	void* arg;
	void (*hook)(void);
	void (*hook_scheduler)(struct rt_thread* from, struct rt_thread* to);
}Thread_control;

typedef struct 
{
	const char* name;
    void (*timeout)(void* parameter);
	void* parameter;
	rt_tick_t time;
	rt_uint8_t flag;
    rt_uint8_t cmd; 
	void* arg;
}Timer_control;

rt_err_t timer_sync(rt_timer_t timer,rt_tick_t time_out);
rt_err_t timer_ctrl(TIMER_CTRL_TYPE Ctrl_type,rt_timer_t timer,Timer_control *timer_param);

rt_err_t thread_ctrl(THREAD_CTRL_TYPE Ctrl_type,rt_thread_t Tid,Thread_control *thread_param);



#endif








