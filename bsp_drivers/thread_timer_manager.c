#include "thread_timer_manager.h"
 
rt_err_t thread_ctrl(THREAD_CTRL_TYPE Ctrl_type,rt_thread_t Tid,Thread_control *thread_param)
{
    rt_err_t ret=RT_EOK;
    switch(Ctrl_type)
    {
    case Ctrl_Thread_Ctreate:
        Tid = rt_thread_create(thread_param->name,
                               thread_param->entry, thread_param->parameter,
                               thread_param->stack_size,
                               thread_param->priority, thread_param->tick);
        if (Tid != RT_NULL)
            ret=RT_EOK;
        break;
    case Ctrl_Thread_Start:
        if (Tid != RT_NULL)
            ret=rt_thread_startup(Tid);
        break;
    case Ctrl_Thread_Delete:
        if (Tid != RT_NULL)
            ret=rt_thread_delete(Tid);
        break;
    case Ctrl_Thread_Suspend:
        if (Tid != RT_NULL)
            ret=rt_thread_suspend(Tid);
        break;
    case Ctrl_Thread_Resume:
        if (Tid != RT_NULL)
            ret=rt_thread_resume(Tid);
        break;
    case Ctrl_Thread_Yield:
        ret=rt_thread_yield();
        break;
    case Ctrl_Thread_GetCurrentThread:
        *thread_param->GetCurrentThread=rt_thread_self();
        break;
    case Ctrl_Thread_Sleep:
        ret=rt_thread_delay(thread_param->delay);
        break;
    case Ctrl_Thread_Control:
        if (Tid != RT_NULL)
            ret=rt_thread_control(Tid,thread_param->cmd,thread_param->arg);
        break;
    case Ctrl_Thread_SetIdleHook:
        if(thread_param->hook!=RT_NULL)
            ret=rt_thread_idle_sethook(thread_param->hook);
        break;
    case Ctrl_Thread_DelIdleHook:
        if(thread_param->hook!=RT_NULL)
            ret=rt_thread_idle_delhook(thread_param->hook);
        break;
    case Ctrl_Thread_SetschedulerHook:
        if(thread_param->hook_scheduler!=RT_NULL)
            rt_scheduler_sethook(thread_param->hook_scheduler);
        break;
    }
    return ret;
}

rt_err_t timer_ctrl(TIMER_CTRL_TYPE Ctrl_type,rt_timer_t timer,Timer_control *timer_param)
{
    rt_err_t ret=RT_EOK;
    switch(Ctrl_type)
    {
    case Ctrl_Timer_Ctreate:
        timer = rt_timer_create(timer_param->name,
                                timer_param->timeout, timer_param->parameter,
                                timer_param->time,
                                timer_param->flag);
        if (timer != RT_NULL)
            ret=RT_EOK;
        break;
    case Ctrl_Timer_Start:
        if (timer != RT_NULL)
            ret=rt_timer_start(timer);
        break;
    case Ctrl_Timer_Stop:
        if (timer != RT_NULL)
            ret=rt_timer_stop(timer);
        break;
    case Ctrl_Timer_Delete:
        if (timer != RT_NULL)
            ret=rt_timer_delete(timer);
        break;
    case Ctrl_Timer_Control:
        if (timer != RT_NULL)
            ret=rt_timer_control(timer,timer_param->cmd,timer_param->arg);
        break;
    }
    return ret;
}


rt_err_t timer_sync(rt_timer_t timer,rt_tick_t time_out)
{
	Timer_control timer_control={0};
	timer_control.cmd=RT_TIMER_CTRL_SET_TIME;
	timer_control.arg=&time;
	timer_ctrl(Ctrl_Timer_Control,timer,&timer_control);
	return timer_ctrl(Ctrl_Timer_Start,timer,&timer_control);
}



















