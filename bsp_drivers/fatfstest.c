#include "fatfs.h"
#include "fatfstest.h"
#include "config.h"
extern FATFS SDFatFS; /* FatFs 文件系统对象 */
extern FIL SDFile;  /* 文件对象 */
FRESULT res_sd; /* 文件操作结果 */
UINT fnum; /* 文件成功读写数量 */
uint8_t is_mount=0;
BYTE ReadBuffer[1024]= {0}; /* 读缓冲区 */
BYTE WriteBuffer[1024] = {0};/* 写缓冲区*/
uint8_t file_init(uint8_t type)
{
	if(is_mount==1) return FR_INT_ERR;
	if(type==0)
	{
		res_sd=f_mount(&SDFatFS,SDPath,1); 					//挂载SD卡 
		if(res_sd==FR_OK&&is_mount==0)
			is_mount=1;
	}
	return (uint8_t)res_sd;
}
uint8_t file_write(uint8_t *file_name,uint8_t *Write_Buffer,uint16_t Write_count)
{
	uint16_t write_num=0;
	res_sd=f_open(&SDFile,(const TCHAR*)file_name,FA_OPEN_ALWAYS|FA_READ|FA_WRITE);
	if(res_sd!=FR_OK) return (uint8_t)res_sd;
	res_sd=f_write(&SDFile,Write_Buffer,(UINT)Write_count,(UINT*)&write_num);
	if(res_sd!=FR_OK||write_num==0) return res_sd;
	res_sd=f_close(&SDFile);
	return res_sd;
}
uint8_t file_DeInit(uint8_t type)
{
	if(is_mount==0) return FR_DISK_ERR;
	if(type==0)
	{
		res_sd=f_mount(&SDFatFS,SDPath,0); 					//挂载SD卡 
		if(res_sd==FR_OK&&is_mount==1)
			is_mount=0;
	}
	else if(type==1)
	{
//		res_sd=f_mount(&SDFatFS,"1:",0); 				//挂载FLASH.
	}
	return (uint8_t)res_sd;

}


uint8_t file_read(uint8_t *file_name,uint8_t *Read_Buffer,uint16_t Read_count,int32_t *readlen)
{
	res_sd=f_open(&SDFile,(const TCHAR*)file_name,FA_OPEN_ALWAYS|FA_READ|FA_WRITE);
	if(res_sd!=FR_OK) return (uint8_t)res_sd;
	res_sd=f_read(&SDFile,Read_Buffer,(UINT)Read_count,(UINT*)readlen);
	if(res_sd!=FR_OK) return res_sd;
	res_sd=f_close(&SDFile);
	return res_sd;
}
uint8_t file_lseek(uint8_t *file_name,uint8_t *Read_Buffer,uint16_t Read_count,uint32_t addr,int32_t *readlen)
{
	int32_t read_num=0;
	res_sd=f_open(&SDFile,(const TCHAR*)file_name,FA_OPEN_ALWAYS|FA_READ|FA_WRITE);	
	res_sd=f_lseek(&SDFile,addr);
	res_sd=f_read(&SDFile,Read_Buffer,(UINT)Read_count,(UINT*)&read_num);
	res_sd=f_close(&SDFile);
	*readlen=read_num;
	return res_sd;
}

uint8_t Creat_NewFile(uint8_t *file_name)
{
	res_sd=f_open(&SDFile,(const TCHAR*)file_name,FA_CREATE_NEW);
	if(res_sd!=FR_OK) return (uint8_t)res_sd;
	res_sd=f_close(&SDFile);
	return res_sd;
}

uint8_t Delete_FileDirs(uint8_t *file_path)
{
	FRESULT res;
    DIR   dir;     /* 文件夹对象 */ //36  bytes
    FILINFO fno;   /* 文件属性 */   //32  bytes
    TCHAR file[_MAX_LFN + 2] = {0};

    
#if _USE_LFN
    fno.fsize = _MAX_LFN;
#endif
    //打开文件夹
    res = f_opendir(&dir, (const TCHAR*)file_path);
    
    //持续读取文件夹内容
    while((res == FR_OK) && (FR_OK == f_readdir(&dir, &fno)))
    {
        //若是"."或".."文件夹，跳过
        if(0 == strlen(fno.fname))          break;      //若读到的文件名为空
        if(0 == strcmp(fno.fname, "."))     continue;   //若读到的文件名为当前文件夹
        if(0 == strcmp(fno.fname, ".."))    continue;   //若读到的文件名为上一级文件夹
        
        memset(file, 0, sizeof(file));
#if _USE_LFN
        sprintf((char*)file, "%s/%s", file_path, (*fno.fname) ? fno.fname : fno.fname);
#else
        sprintf((char*)file, "%s/%s", file_path, fno.fname);
#endif
        if (fno.fattrib & AM_DIR)
        {//若是文件夹，递归删除
            res = (FRESULT)Delete_FileDirs((uint8_t*)file);
        }
        else
        {//若是文件，直接删除
            res = f_unlink(file);
        }
    }
    
    //删除本身
    if(res == FR_OK)    res = f_unlink((TCHAR*)file_path);
    
    return res;
}
uint8_t File_Mkdirs(uint8_t *dirs_name)
{
	res_sd= f_mkdir((const TCHAR*)dirs_name);
	return (uint8_t)res_sd;
}






