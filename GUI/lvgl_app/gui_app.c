#include "gui_app.h"
#include "stdint.h"
#include "stdbool.h"
#include "string.h"
#include "rtthread.h"
#include "lvgl.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"
#include "lv_examples.h"
#include "bsp_lcd.h"
#include "bsp_touch.h"
#include "bsp_tim.h"
lv_obj_t* btn1;
#define device  struct rt_device
//事件回调函数
static void btn_event_cb(lv_obj_t* obj, lv_event_t event)
{
    if (event == LV_EVENT_RELEASED)
    {
        if (obj == btn1)
        {
            rt_kprintf("anxiale ");
        }

             
    }
}
//例程入口函数
void lv_btn_test_start()
{
    //lv_obj_t* obj = lv_obj_create(lv_scr_act(), NULL);
    lv_obj_t* src = lv_scr_act();//获取当前活跃的屏幕对象
        btn1 = lv_btn_create(src, NULL);
    lv_obj_set_pos(btn1, 20, 20);//设置坐标
    lv_obj_set_event_cb(btn1, btn_event_cb);
}
rt_thread_t tid1 = RT_NULL;
static void thread1_entry1(void *parameter)
{
	lcd_dev.show_zi_addr=&show_zi_addr;
	register_timcallbck((uint32_t)lv_tick_inc,1);
	TIM3_Init(999,83);
	LCD_Init();
	LCD_Display_Dir(0);
	lv_init();						//lvgl系统初始化
	lv_port_disp_init();	//lvgl显示接口初始化,放在lv_init()的后面
	lv_port_indev_init();	//lvgl输入接口初始化,放在lv_init()的后面
	lv_btn_test_start();
    while (1)
    {
		tp_dev.scan(0);
		lv_task_handler();
		rt_thread_mdelay(1);
    }
}
rt_err_t lvgl_thread(device *dev)
{
	 tid1 = rt_thread_create("lvgl",
                            thread1_entry1, RT_NULL,
                            2500,
                            5, 10);

    /* 如果获得线程控制块，启动这个线程 */
    if (tid1 != RT_NULL)
        rt_thread_startup(tid1);
	return RT_EOK;
}
rt_err_t lvgl_Deinit(device *dev)
{
	LCD_DeInit();
	return RT_EOK;
}

device lvgl_dev={0};
int Register_lvgl_dev()
{
	lvgl_dev.init=lvgl_thread;
	lvgl_dev.close=lvgl_Deinit;
	lvgl_dev.user_data=&lcd_dev;
	return rt_device_register(&lvgl_dev,"LVGL",RT_DEVICE_FLAG_RDWR);
}

DEVICE_INIT(Register_lvgl_dev);






