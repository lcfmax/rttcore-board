#include "Dev_usart.h"

#define uart2_name "uart2"
#define uart3_name "uart3"

#define uart2_rx_sem "rxsem2"
#define uart3_rx_sem "rxsem3"


#define uart2_rx_timer "rx2timer"
#define uart3_rx_timer "rx3timer"

#define UART2_RX_TIMEOUT 100000
#define UART3_RX_TIMEOUT 100000
DEV_UART	Dev_uart2;
DEV_UART	Dev_uart3;

static void uart_rx_timer_callback(void* parameter);

static void uart2_rx_thread_entry(void* parameter);
static void uart3_rx_thread_entry(void* parameter);

static  rt_err_t uart_rxcallback(rt_device_t dev, rt_size_t size);


rt_err_t Dev_Uart_Init(uint8_t index)
{
	rt_err_t ret=RT_EOK;
	switch (index)
    {
    	case 2:
			Dev_uart2.Dev_Uart=rt_device_find(uart2_name);
			if(Dev_uart2.Dev_Uart==RT_NULL)
			{
				rt_kprintf("串口2查找失败\r\n");
				return RT_ERROR;
			}
			Dev_uart2.rx_sem=rt_sem_create(uart2_rx_sem,0,RT_IPC_FLAG_FIFO);	
			
			Dev_uart2.rx_time_out=UART2_RX_TIMEOUT;
			Dev_uart2.rx_time_out_timer=rt_timer_create(uart2_rx_timer,uart_rx_timer_callback,
														&Dev_uart2.rx_frame,Dev_uart2.rx_time_out,
														RT_TIMER_FLAG_ONE_SHOT | RT_TIMER_FLAG_SOFT_TIMER);
			if(Dev_uart2.rx_time_out_timer==RT_NULL)
			{
				rt_kprintf("串口2定时器创建失败\r\n");
				return ret;
			}											
			ret=rt_device_open(Dev_uart2.Dev_Uart, RT_DEVICE_FLAG_INT_RX|RT_DEVICE_FLAG_INT_TX|RT_DEVICE_OFLAG_RDWR);
			if(ret!=RT_EOK)
			{
				rt_kprintf("串口2打开失败\r\n");
				return ret;
			}
			rt_device_set_rx_indicate(Dev_uart2.Dev_Uart,uart_rxcallback);
			
			Dev_uart2.rx_ringbuf=rt_ringbuffer_create(UART2_RINGBUFFER_MAX);
			
			Dev_uart2.rx_thread_tid = rt_thread_create("uart2_rx",
														 uart2_rx_thread_entry, RT_NULL,
														 512,
														 5, 10);
			if(Dev_uart2.rx_thread_tid==NULL)
			{
				rt_kprintf("串口2线程创建失败\r\n");
				return RT_ERROR;
			}
			rt_thread_startup(Dev_uart2.rx_thread_tid);
    		break;
    	case 3:
			Dev_uart3.Dev_Uart=rt_device_find(uart3_name);
			if(Dev_uart3.Dev_Uart==RT_NULL)
			{
				rt_kprintf("串口3查找失败\r\n");
				return RT_ERROR;
			}
			Dev_uart3.rx_sem=rt_sem_create(uart3_rx_sem,0,RT_IPC_FLAG_FIFO);	
			
			Dev_uart3.rx_time_out=UART3_RX_TIMEOUT;
			
			Dev_uart3.rx_time_out_timer=rt_timer_create(uart3_rx_timer,uart_rx_timer_callback,
														&Dev_uart3.rx_frame,Dev_uart3.rx_time_out,
														RT_TIMER_FLAG_ONE_SHOT | RT_TIMER_FLAG_SOFT_TIMER);
			if(Dev_uart3.rx_time_out_timer==RT_NULL)
			{
				rt_kprintf("串口3定时器创建失败\r\n");
				return ret;
			}											
			ret=rt_device_open(Dev_uart3.Dev_Uart, RT_DEVICE_FLAG_INT_RX|RT_DEVICE_FLAG_INT_TX|RT_DEVICE_OFLAG_RDWR);
			if(ret!=RT_EOK)
			{
				rt_kprintf("串口3打开失败\r\n");
				return ret;
			}
			rt_device_set_rx_indicate(Dev_uart3.Dev_Uart,uart_rxcallback);
			
			Dev_uart3.rx_ringbuf=rt_ringbuffer_create(UART3_RINGBUFFER_MAX);
			
			Dev_uart3.rx_thread_tid = rt_thread_create("uart3_rx",
														 uart3_rx_thread_entry, RT_NULL,
														 512,
														 5, 10);
			if(Dev_uart3.rx_thread_tid==NULL)
			{
				rt_kprintf("串口3线程创建失败\r\n");
				return RT_ERROR;
			}
			rt_thread_startup(Dev_uart3.rx_thread_tid);
    		break;
    	default:
    		break;
    }
	return ret;
}
void uart_rx_timer_callback(void* parameter)
{
	*(uint8_t*)(parameter)=1;
}
static  rt_err_t uart_rxcallback(rt_device_t dev, rt_size_t size)
{
	DEV_UART *uart_dev;
	uart_dev=(DEV_UART *)dev;
	rt_timer_start(uart_dev->rx_time_out_timer);
	rt_sem_release(uart_dev->rx_sem);
	return RT_EOK;
}
static void uart2_rx_thread_entry(void* parameter)
{
	rt_uint8_t ch;
	while(1)
	{
		while (rt_device_read(Dev_uart2.Dev_Uart, -1, &ch, 1) != 1)
		{
			/* 阻 塞 等 待 接 收 信 号 量， 等 到 信 号 量 后 再 次读 取 数 据 */
			rt_sem_take(Dev_uart2.rx_sem, RT_WAITING_FOREVER);
		}
		rt_ringbuffer_put_force(Dev_uart2.rx_ringbuf,&ch,1);
	}
}

static void uart3_rx_thread_entry(void* parameter)
{
	rt_uint8_t ch;
	while(1)
	{
		while (rt_device_read(Dev_uart3.Dev_Uart, -1, &ch, 1) != 1)
		{
			/* 阻 塞 等 待 接 收 信 号 量， 等 到 信 号 量 后 再 次读 取 数 据 */
			rt_sem_take(Dev_uart3.rx_sem, RT_WAITING_FOREVER);
		}
		rt_ringbuffer_put_force(Dev_uart3.rx_ringbuf,&ch,1);
	}
}

rt_err_t Uart_Write(rt_device_t uart_dev,uint8_t *buf,uint32_t len)
{
	int tx_len=0;
	tx_len=rt_device_write(uart_dev,0,buf,len);
	if(tx_len==len)
	{
		return RT_EOK;
	}
	else
	{
		rt_kprintf("串口%s发送失败\r\n",uart_dev->parent.name);
		return RT_ERROR;
	}
}
uint32_t Uart_Read(rt_device_t uart_dev,uint8_t *buf,uint16_t len)
{
	DEV_UART *dev;
	dev=(DEV_UART *)uart_dev;
	uint32_t buf_len=rt_ringbuffer_data_len(dev->rx_ringbuf);
	if(buf_len<len)
		len=buf_len;
	buf_len=rt_ringbuffer_get(dev->rx_ringbuf,buf,len);
	if(rt_ringbuffer_data_len(dev->rx_ringbuf)==0)
		dev->rx_frame=0;	
	return buf_len;
}

uint8_t Uart_Get_Rx_State(rt_device_t uart_dev,uint16_t *rx_len)
{
	DEV_UART *dev;
	dev=(DEV_UART *)uart_dev;
	*rx_len=rt_ringbuffer_data_len(dev->rx_ringbuf);
	return dev->rx_frame;
}



