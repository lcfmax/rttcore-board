#ifndef __DEV_USART_H
#define __DEV_USART_H
#include "rtthread.h"
#include "ipc/ringbuffer.h"
typedef struct
{
	rt_device_t Dev_Uart;
	rt_thread_t rx_thread_tid;
	rt_tick_t   rx_time_out;
	rt_timer_t	rx_time_out_timer;		//接收成帧超时定时器
	rt_sem_t 	rx_sem;
	uint8_t 	rx_frame;				//接收成帧标志
	struct      rt_ringbuffer *rx_ringbuf;
}DEV_UART;	


#define UART2_RINGBUFFER_MAX 1200
#define UART3_RINGBUFFER_MAX 500

rt_err_t Dev_Uart_Init(uint8_t index);
rt_err_t Uart_Write(rt_device_t uart_dev,uint8_t *buf,uint32_t len);
uint32_t Uart_Read(rt_device_t uart_dev,uint8_t *buf,uint16_t len);
uint8_t  Uart_Get_Rx_State(rt_device_t uart_dev,uint16_t *rx_len);


#endif









